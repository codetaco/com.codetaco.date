package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateHourTest {
    @Test
    public void add_hour() {
        CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                    "=1h",
                                                    "+1hour");
    }

    @Test
    public void add_invalidBeginning() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                        "",
                                                        "+bhour");
            Assertions.fail("expected an exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void atBeforeCurrentTimeResultsInPastTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "=9hours",
                                                    "=9hours");
    }

    @Test
    public void atEndOfHourWhenAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =13hours",
                                                    "",
                                                    "=ehour");
    }

    @Test
    public void atEndOfHourWhenNotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =13hours",
                                                    "=et =13hour",
                                                    "=ehours");
    }

    @Test
    public void bug_nextOrThisAfterTheTimeInTheCurrentDay() {
        CalendarFactoryHelper.startExpectedComputed("=bt =20hours =45minutes",
                                                    "=12h =00i",
                                                    "+0d =btime >11h >0min");
    }

    @Test
    public void bug_nextOrThisAfterTheTimeInTheCurrentDayAtBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=bt =20hours =45minutes",
                                                    "+1d =11h =00i",
                                                    "+0d >11h =bhour");
    }

    @Test
    public void bug_nextOrThisAfterTheTimeInTheCurrentDayNoBTIME() {
        CalendarFactoryHelper.startExpectedComputed("=bt =20hours =45minutes",
                                                    "+1d =12h =00i",
                                                    "+0d >11h >0min");
    }

    @Test
    public void greaterBeforeCurrentTimeResultsInFutureTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "+1d =9hours",
                                                    ">9hours");
    }

    @Test
    public void itsBeginningOfHour_prevOrThis() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours",
                                                    "",
                                                    "<=bhour");
    }

    @Test
    public void itsFirstHour_nextOrThisMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "=bt =2h",
                                                    ">=bhour");
    }

    @Test
    public void itsFirstHour_prevOrThisMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "=bt =1h",
                                                    "<=bhour");
    }

    @Test
    public void itsJustAfterMidnightHour_thisMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                    "=bt",
                                                    "<bhour");
    }

    @Test
    public void itsMidnight_prevHour() {
        CalendarFactoryHelper.startExpectedComputed("=bt",
                                                    "-1day =bt =23h",
                                                    "<bhour");
    }

    @Test
    public void itsMidnightHour_nextMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt =0hours =0minutes",
                                                    "=bt =1h",
                                                    ">bhour");
    }

    @Test
    public void itsMidnightHour_nextOrThisMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt",
                                                    "",
                                                    ">=bhour");
    }

    @Test
    public void itsNoonHour_at5pm() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "=bt +17hours =10minutes",
                                                    "=17hours =10minutes");
    }

    @Test
    public void itsNoonHour_atMidnight() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "=bt =12h",
                                                    "=bh");
    }

    @Test
    public void lessAfterTimeResultsInPastTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "-1d =13hours",
                                                    "<13hours");
    }

    @Test
    public void lessBeforeTimeResultsInExactTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "=9hours",
                                                    "<9hours");
    }

    @Test
    public void lessOrEquals_equals() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "=0min",
                                                    "<=12hours");
    }

    @Test
    public void lessOrEquals_greater() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "-1d =13h =0min",
                                                    "<=13hours");
    }

    @Test
    public void nextEnd_AtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =12hours",
                                                    "=13h",
                                                    ">eh");
    }

    @Test
    public void nextEnd_NotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "=et =12h",
                                                    ">eh");
    }

    @Test
    public void nextOrThisEnd_AtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =12hours",
                                                    "",
                                                    ">=eh");
    }

    @Test
    public void nextOrThisEnd_NotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =10minutes",
                                                    "=et =12h",
                                                    ">=eh");
    }

    @Test
    public void nextOrThisHour_next() {
        CalendarFactoryHelper.startExpectedComputed("=bt =2hours =10minutes",
                                                    "+1d =1h =0i",
                                                    ">=1hour");
    }

    @Test
    public void nextOrThisHour_this() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "=1h =0i",
                                                    ">=1hour");
    }

    @Test
    public void prevEnd_AtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =12hours",
                                                    "=et =11h",
                                                    "<eh");
    }

    @Test
    public void prevEnd_NotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "=et =11h",
                                                    "<eh");
    }

    @Test
    public void prevOrThisEnd_AtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =12hours",
                                                    "",
                                                    "<=eh");
    }

    @Test
    public void prevOrThisEnd_NotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours",
                                                    "=et =11h",
                                                    "<=eh");
    }

    @Test
    public void subtract_hour() {
        CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                    "-1d =23h",
                                                    "-1hour");
    }

    @Test
    public void subtract_invalidBeginning() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                        "",
                                                        "-bhour");
            Assertions.fail("expected an exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }
}
