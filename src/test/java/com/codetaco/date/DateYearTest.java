package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateYearTest {
    public DateYearTest() {

    }

    @Test
    public void absoluteBeginningYear_next() {
        CalendarFactoryHelper.startExpectedComputed("=2d =1M =2011y =btime",
                                                    "=1d =1m =2011year",
                                                    "=byear");
    }

    @Test
    public void absoluteEndYear_next() {
        CalendarFactoryHelper.startExpectedComputed("=2d =1M =2011y =btime",
                                                    "=12m =31d =2011year =etime",
                                                    "=eyear");
    }

    @Test
    public void addBeginningYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "+byear");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void addEndingYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "+eyear");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void addYear() {
        CalendarFactoryHelper.startExpectedComputed("=2011y =10M =19d",
                                                    "=2013y",
                                                    "+2year");
    }

    @Test
    public void nextBeginningYear() {
        CalendarFactoryHelper.startExpectedComputed("=1d =1M =2011y =btime",
                                                    "=1d =1m =2012y",
                                                    ">byear");
    }

    @Test
    public void nextEndOfYear() {
        CalendarFactoryHelper.startExpectedComputed("=1d =1M =2011y =btime",
                                                    "=12m =31d =2011y =etime",
                                                    ">eyear");
    }

    @Test
    public void nextOrThisBeginningYear_next() {
        CalendarFactoryHelper.startExpectedComputed("=2d =1M =2011y =btime",
                                                    "=1d =1m =2012year",
                                                    ">=byear");
    }

    @Test
    public void nextOrThisBeginningYear_this() {
        CalendarFactoryHelper.startExpectedComputed("=1d =1M =2011y =btime",
                                                    "",
                                                    ">=byear");
    }

    @Test
    public void nextOrThisEndOfYear() {
        CalendarFactoryHelper.startExpectedComputed("=12M =31d =2011y",
                                                    "=etime",
                                                    ">=eyear");
    }

    @Test
    public void nextOrThisYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        ">=1year");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: NEXTORTHIS",
                                    e.getMessage());
        }
    }

    @Test
    public void nextThisYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        ">1year");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: NEXT",
                                    e.getMessage());
        }
    }

    @Test
    public void prevBeginningYear() {
        CalendarFactoryHelper.startExpectedComputed("=2d =1M =2011y =btime",
                                                    "=1d =2011y",
                                                    "<byear");
    }

    @Test
    public void prevEndOfYear_next() {
        CalendarFactoryHelper.startExpectedComputed("=12M =30d =2011y",
                                                    "=31d -1year =etime",
                                                    "<eyear");
    }

    @Test
    public void prevEndOfYear_this() {
        CalendarFactoryHelper.startExpectedComputed("=12M =31d =2011y",
                                                    "-1year =etime",
                                                    "<eyear");
    }

    @Test
    public void prevOrThisBeginningYear_prev() {
        CalendarFactoryHelper.startExpectedComputed("=2d =1M =2011y =btime",
                                                    "=1d =1m =2011year",
                                                    "<=byear");
    }

    @Test
    public void prevOrThisEndOfYear_prev() {
        CalendarFactoryHelper.startExpectedComputed("=12M =30d =2011y",
                                                    "=31d -1year =etime",
                                                    "<=eyear");
    }

    @Test
    public void prevOrThisEndOfYear_this() {
        CalendarFactoryHelper.startExpectedComputed("=12M =31d =2011y =et",
                                                    "",
                                                    "<=eyear");
    }

    @Test
    public void prevOrThisYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "<=1year");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: PREVORTHIS",
                                    e.getMessage());
        }
    }

    @Test
    public void prevThisYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "<1year");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: PREV",
                                    e.getMessage());
        }
    }

    @Test
    public void subtractBeginningYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "-byear");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }

    @Test
    public void subtractEndingYear_ERROR() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=2011y =10M =1d =0ms",
                                                        "",
                                                        "-eyear");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }

    @Test
    public void subtractYear() {
        CalendarFactoryHelper.startExpectedComputed("=2011y =10M =19d", "=2009y", "-2year");
    }
}
