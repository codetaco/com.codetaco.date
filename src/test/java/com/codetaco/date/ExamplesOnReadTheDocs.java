package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ExamplesOnReadTheDocs {

    @Test
    public void date_and_time_for_right_now() {
        CalendarFactory.setBusinessDate("1960-04-09T12:15:47.321Z");
        Assertions.assertEquals("1960-04-09T12:15:47.321Z",
                                CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void date_and_time_rounded_down_to_the_hour() {
        Assertions.assertEquals("1960-04-09T12:00:00Z",
                                CalendarFactory.asJSON("1960-04-09T12:15:47.321Z",
                                                       "=bhour"));
    }

    @Test
    public void date_and_time_in_Chicago() {
        CalendarFactory.setBusinessDate("1960-04-09T12:15:47.321Z");
        ZonedDateTime result = CalendarFactory.asZoned("now").withZoneSameInstant(ZoneId.of("America/Chicago"));
        Assertions.assertEquals("1960-04-09T06:15:47.321-06:00[America/Chicago]", CalendarFactory.asJSON(result));
    }

    @Test
    public void chicago_time_from_Hydrabad() {
        String databaseDateTimeField = "2018-10-20T10:00+05:30";
        ZonedDateTime dateTime = CalendarFactory.asZoned(databaseDateTimeField);
        ZonedDateTime chicagoDateTime = dateTime.withZoneSameInstant(ZoneId.of("America/Chicago"));
        LocalTime result = CalendarFactory.asLocalTime(chicagoDateTime);
        Assertions.assertEquals("23:30:00", CalendarFactory.asJSON(result));
    }
}
