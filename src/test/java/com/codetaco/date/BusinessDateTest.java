package com.codetaco.date;

import com.codetaco.date.impl.TemporalHelper;
import com.codetaco.date.impl.helper.ITemporalHelperImpl;
import com.codetaco.date.impl.helper.TemporalHelperEUImpl;
import org.junit.jupiter.api.Test;

public class BusinessDateTest {
    @Test
    public void largeNegative() {
        CalendarFactoryHelper.startExpectedComputed("=2011year =1month =1day =btime",
                                                    "=2011year =1month =1day =btime",
                                                    "=btime");
    }

    @Test
    public void largePositive() {
        CalendarFactoryHelper.startExpectedComputed("=2111year =1month =1day =btime",
                                                    "=2111year =1month =1day =btime",
                                                    "=btime");
    }

    @Test
    public void smallNegative() {
        CalendarFactoryHelper.startExpectedComputed("=2011year =10month =20day =btime",
                                                    "=2011year =10month =20day =btime",
                                                    "=btime");
    }

    @Test
    public void smallPositive() {
        CalendarFactoryHelper.startExpectedComputed("=2011year =12month =31day =btime",
                                                    "=2011year =12month =31day =btime",
                                                    "=btime");
    }

    @Test
    public void specialDateMMM_default() throws Exception {
        TemporalHelper.parseWithPredefinedParsers("Jul 4, 1776");
    }

    @Test
    public void specialDateMMM_EU() throws Exception {
        ITemporalHelperImpl previous = TemporalHelper.setDelegate(new TemporalHelperEUImpl());
        TemporalHelper.parseWithPredefinedParsers("4 Jul, 1776");
        TemporalHelper.setDelegate(previous);
    }
}
