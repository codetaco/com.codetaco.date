package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateDayOfWeekTest {
    public DateDayOfWeekTest() {

    }

    @Test
    public void at_BOW() {
        CalendarFactoryHelper.startExpectedComputed("=2dayofweek =20ms",
                                                    "-1d =20ms",
                                                    "=bdayofweek");
    }

    @Test
    public void at_EOW() {
        CalendarFactoryHelper.startExpectedComputed("=1dayofweek =20ms",
                                                    "+6d =23h =59mi =59s =999999999n",
                                                    "=edayofweek", true);
    }

    @Test
    public void next_BOW_NextDayInNextWeek() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "+7d =1dayofweek",
                                                    ">bdayofweek");
    }

    @Test
    public void next_DAY_NextDayInNextWeek() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "+7d",
                                                    ">6dayofweek");
    }

    @Test
    public void next_DAY_NextDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=5dayofweek =btime"
          , "+1d",
                                                    ">6dayofweek");
    }

    @Test
    public void next_EOW_NextDayInNextWeek() {
        CalendarFactoryHelper.startExpectedComputed("=7dayofweek =etime",
                                                    "+7d =7dayofweek =etime",
                                                    ">edayofweek");
    }

    @Test
    public void nextEq_BOW_NextDayInNextWeek() {
        CalendarFactoryHelper.startExpectedComputed("=2dayofweek =btime",
                                                    "+7d =1dayofweek",
                                                    ">=bdayofweek");
    }

    @Test
    public void nextEq_BOW_NextDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=1dayofweek =btime",
                                                    "",
                                                    ">=bdayofweek");
    }

    @Test
    public void nextEq_DAY_NextDayInNextWeek() {
        CalendarFactoryHelper.startExpectedComputed("=5dayofweek =btime",
                                                    "+7d -1d",
                                                    ">=4dayofweek");
    }

    @Test
    public void nextEq_DAY_NextDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=5dayofweek =btime",
                                                    "+1d",
                                                    ">=6dayofweek");
    }

    @Test
    public void nextEq_DAY_ThisDayIsOk() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "",
                                                    ">=6dayofweek");
    }

    @Test
    public void nextEq_EOW_NextDayInSameWeek() {
        /*
         * Wed=3, SUN=7
         */
        CalendarFactoryHelper.startExpectedComputed("=3dayofweek ",
                                                    "=7dayofweek =etime",
                                                    ">=edayofweek");
    }

    @Test
    public void nextEq_EOW_NextDayIsOK() {
        CalendarFactoryHelper.startExpectedComputed("=7dayofweek ",
                                                    "=etime",
                                                    ">=edayofweek");
    }

    @Test
    public void plus_BOW_error() {
        try {
            CalendarFactoryHelper.startExpectedComputed("",
                                                        "",
                                                        "+bdayofweek");
            Assertions.fail("expected invalid direction in data adjustment: ADD");

        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plus_DayOfWeek() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=btime",
                                                        "+24Hours",
                                                        "+1dayofweek");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void plus_EOW_error() {
        try {
            CalendarFactoryHelper.startExpectedComputed("",
                                                        "",
                                                        "+edayofweek");
            Assertions.fail("expected invalid direction in data adjustment: ADD");

        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void prev_BOW_PrevDayInPrevWeek() {
        CalendarFactoryHelper.startExpectedComputed("=1dayofweek =btime",
                                                    "-7d =et",
                                                    "<bdayofweek");
    }

    @Test
    public void prev_BOW_PrevDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=2dayofweek =btime",
                                                    "-1d =et",
                                                    "<bdayofweek");
    }

    @Test
    public void prev_DAY_PrevDayInPrevWeek() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "-7d =et",
                                                    "<6dayofweek");
    }

    @Test
    public void prev_DAY_PrevDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "-1d =et",
                                                    "<5dayofweek");
    }

    @Test
    public void prev_EOW_PrevDayInPrevWeek() {
        CalendarFactoryHelper.startExpectedComputed("=7dayofweek",
                                                    "-7d =etime",
                                                    "<edayofweek");
    }

    @Test
    public void prevEq_BOW_PrevDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=2dayofweek =btime",
                                                    "-1d =et",
                                                    "<=bdayofweek");
    }

    @Test
    public void prevEq_BOW_ThisDayIsOk() {
        CalendarFactoryHelper.startExpectedComputed("=1dayofweek =btime",
                                                    "-7d =et",
                                                    "<=bdayofweek");
    }

    @Test
    public void prevEq_DAY_PrevDayInPrevWeek() {
        CalendarFactoryHelper.startExpectedComputed("=4dayofweek =btime",
                                                    "-7d +1d =et",
                                                    "<=5dayofweek");
    }

    @Test
    public void prevEq_DAY_PrevDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "-1d =et",
                                                    "<=5dayofweek");
    }

    @Test
    public void prevEq_DAY_ThisDayIsOk() {
        CalendarFactoryHelper.startExpectedComputed("=6dayofweek =btime",
                                                    "=et",
                                                    "<=6dayofweek");
    }

    @Test
    public void prevEq_EOW_PrevDayInSameWeek() {
        CalendarFactoryHelper.startExpectedComputed("=7dayofweek",
                                                    "=etime",
                                                    "<=edayofweek");
    }

    @Test
    public void prevEq_EOW_ThisDayIsOk() {
        CalendarFactoryHelper.startExpectedComputed("=7dayofweek",
                                                    "=etime",
                                                    "<=edayofweek");
    }

    @Test
    public void subtract_BOW_error() {
        try {
            CalendarFactoryHelper.startExpectedComputed("",
                                                        "",
                                                        "-bdayofweek");
            Assertions.fail("expected invalid direction in data adjustment: SUBTRACT");

        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }

    @Test
    public void subtract_DayOfWeek() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=btime",
                                                        "-1day",
                                                        "-1dayofweek");
        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }

    @Test
    public void subtract_EOW_error() {
        try {
            CalendarFactoryHelper.startExpectedComputed("",
                                                        "",
                                                        "-edayofweek");
            Assertions.fail("expected invalid direction in data adjustment: SUBTRACT");

        } catch (Exception e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT",
                                    e.getMessage());
        }
    }
}
