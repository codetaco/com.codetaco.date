package com.codetaco.date;

import com.codetaco.date.impl.TemporalHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TemporalHelperTests {

    private void verify(String input,
                        DateTimeFormatter formatter,
                        String output) {
        try {
            ZonedDateTime localDateTime = TemporalHelper.parseWithPredefinedParsers(input);
//            TemporalHelper.usageReport().forEach(System.out::println);
            Assertions.assertEquals(output, formatter.format(localDateTime));
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void test006() {
        verify("04-09-1960",
               TemporalHelper.getOutputDTF(),
               "1960-04-09T00:00:00Z");
    }

    @Test
    public void test011() {
        verify("1960-04-09",
               TemporalHelper.getOutputDF(),
               "1960-04-09Z");
    }

    @Test
    public void test016() {
        verify("1960-04",
               TemporalHelper.getOutputDF(),
               "1960-04-01Z");
    }

    @Test
    public void test021() {
        verify("04-1960",
               TemporalHelper.getOutputDF(),
               "1960-04-01Z");
    }

    @Test
    public void test026() {
        verify("21:15:45.123",
               TemporalHelper.getOutputTF(),
               "21:15:45.123Z");
    }

    @Test
    public void test027() {
        verify("21:15:45",
               TemporalHelper.getOutputTF(),
               "21:15:45Z");
    }

    @Test
    public void test028() {
        verify("21:15",
               TemporalHelper.getOutputTF(),
               "21:15:00Z");
    }
}
