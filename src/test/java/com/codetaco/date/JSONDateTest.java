package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

public class JSONDateTest {
    @Test
    public void ISO_DATE_TIME6() {
        ZonedDateTime ldt = CalendarFactory.asZoned("2013-03-11T01:38:18.309Z");
        Assertions.assertEquals(
          "=2013year =3month =11day =1hour =38minute =18second =309000000nanosecond",
          CalendarFactory.asFormula(ldt));

//        final String jsonFormat = CalendarFactory.asJSON(ldt);
//        Assertions.assertEquals("JSON format", "2013-03-11T01:38:18.309", jsonFormat.substring(0, 23));
    }

    @Test
    public void ISO_DATE_TIME() {
        ZonedDateTime ldt = CalendarFactory.asZoned("2013-03-11T01:38:18.309+00:00");
        Assertions.assertEquals(
          "=2013year =3month =11day =1hour =38minute =18second =309000000nanosecond",
          CalendarFactory.asFormula(ldt));
    }

    @Test
    public void ISO_DATE_TIME1() {
        ZonedDateTime ldt = CalendarFactory.asZoned("2013-03-11T01:38:18+00:00");
        Assertions.assertEquals(
          "=2013year =3month =11day =1hour =38minute =18second =0nanosecond",
          CalendarFactory.asFormula(ldt));
    }

    @Test
    public void ISO_DATE_TIME2() {
        ZonedDateTime ldt = CalendarFactory.asZoned("2013-03-11T01:38+00:00");
        Assertions.assertEquals(
          "=2013year =3month =11day =1hour =38minute =0second =0nanosecond",
          CalendarFactory.asFormula(ldt));
    }

    @Test
    public void ISO_DATE_TIME3() {
        try {
            CalendarFactory.asZoned("2013-03-11T01:38.123+00:00");
            Assertions.fail("Expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("not in a predefined date / time format (2013-03-11T01:38.123+00:00)",
                                    e.getMessage());
        }
    }

    @Test
    public void ISO_DATE_TIME4() {
        try {
            CalendarFactory.asZoned("2013-03-11T01+00:00");
            Assertions.fail("Expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("not in a predefined date / time format (2013-03-11T01+00:00)",
                                    e.getMessage());
        }
    }

    @Test
    public void ISO_DATE_TIME5() {
        try {
            CalendarFactory.asZoned("2013-03-11T01.123+00:00");
            Assertions.fail("Expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("not in a predefined date / time format (2013-03-11T01.123+00:00)",
                                    e.getMessage());
        }
    }

    @Test
    public void BASIC_ISO_DATE1() {
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "20130311")));
    }

    @Test
    public void BASIC_ISO_DATE2() {
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "20130311Z")));
    }

    @Test
    public void BASIC_ISO_DATE3() {
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "20130311+0030")));
    }

    @Test
    public void ISO_LOCAL_DATE() {
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2013-03-11")));
    }

    @Test
    public void ISO_OFFSET_DATE() {
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2013-03-11+00:00")));
    }

    @Test
    public void now() {
        CalendarFactory.setBusinessDate("2013-03-11T10:20:30.400Z");
        Assertions.assertEquals("2013-03-11T10:20:30.4Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "now")));
    }

    @Test
    public void today() {
        CalendarFactory.setBusinessDate("2013-03-11T10:20:30.400Z");
        Assertions.assertEquals("2013-03-11T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "today")));
    }
}
