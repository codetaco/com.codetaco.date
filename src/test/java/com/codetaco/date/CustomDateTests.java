package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CustomDateTests {

    @Test
    public void MMMdyyy() {
        Assertions.assertEquals("1776-07-04T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "Jul 4, 1776")));
    }

    @Test
    public void dMMMyyyy() {
        Assertions.assertEquals("1776-07-04T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "4 Jul, 1776")));
    }

    @Test
    public void Myyyy() {
        Assertions.assertEquals("1776-07-01T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "7-1776")));
    }

    @Test
    public void yyyyM() {
        Assertions.assertEquals("1776-07-01T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1776-7")));
    }

    @Test
    public void MMddyyyy() {
        Assertions.assertEquals("1776-07-04T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "07-04-1776")));
    }

    @Test
    public void yyyyMMdd() {
        Assertions.assertEquals("1776-07-04T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1776-07-04")));
    }

    @Test
    public void yyyy() {
        Assertions.assertEquals("1776-01-01T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1776")));
    }

    @Test
    public void jsonNoZone1() {
        Assertions.assertEquals("1997-07-16T19:20:01Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1997-07-16T19:20:01")));
    }

    @Test
    public void jsonCompressed() {
        Assertions.assertEquals("1997-07-16T19:20:01Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "19970716T192001")));
    }

    @Test
    public void jsonNoZone2() {
        Assertions.assertEquals("1997-07-16T19:20:01Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1997-07-16T19:20:01")));
    }

    @Test
    public void json4() {
        Assertions.assertEquals("1997-07-16T19:20:00+01:00",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1997-07-16T19:20+01:00")));
    }

    @Test
    public void json5() {
        Assertions.assertEquals("1997-07-16T19:20:30+01:00",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "1997-07-16T19:20:30+01:00")));
    }

    @Test
    public void zoned() {
        Assertions.assertEquals("2011-12-03T10:15:30+01:00[Europe/Paris]",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2011-12-03T10:15:30+01:00[Europe/Paris]")));
    }

    @Test
    public void weekNumberWithoutDayOfWeek() {
        Assertions.assertEquals("2018-10-15T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2018-W42")));
    }

    @Test
    public void weekNumberWithDayOfWeek() {
        Assertions.assertEquals("2018-10-21T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2018-W42-7")));
    }

    @Test
    public void ordinalDate() {
        Assertions.assertEquals("2018-10-21T00:00:00Z",
                                CalendarFactory.asJSON(
                                  CalendarFactory.asZoned(
                                    "2018-294")));
    }
}
