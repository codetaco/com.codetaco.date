package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateParserErrorTest {

    @Test
    public void directionIsRequired() {
        try {
            CalendarFactory.asZoned("now", "2010year");
            Assertions.fail("should have been exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction: 2010year", e.getMessage());
        }

    }

    @Test
    public void invalidUOM() {
        try {
            CalendarFactory.asZoned("now", "+1xx");
            Assertions.fail("should have been exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid uom: xx", e.getMessage());
        }

    }

    @Test
    public void missingQTY() {
        try {
            CalendarFactory.asZoned("now", "+xx");
            Assertions.fail("should have been exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("missing qty in +xx", e.getMessage());
        }

    }

    @Test
    public void spaceForQty() {
        try {
            CalendarFactory.asZoned("now", "+ d");
            Assertions.fail("should have been exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("Premature end of formula, qty expected: +", e.getMessage());
        }

    }

    @Test
    public void uomIsRequired() {
        try {
            CalendarFactory.asZoned("now", "=2010");
            Assertions.fail("should have been exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("uom is required", e.getMessage());
        }

    }

}
