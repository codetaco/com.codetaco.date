package com.codetaco.date;

import org.junit.jupiter.api.Assertions;

import java.time.ZonedDateTime;

public class CalendarFactoryHelper {

    static public void startExpectedComputed(String startingDateCommand,
                                             String expectedDateCommand,
                                             String computedDateCommand) {
        startExpectedComputed(startingDateCommand,
                              expectedDateCommand,
                              computedDateCommand,
                              false);
    }

    static public void startExpectedComputed(String startingDateCommand,
                                             String expectedDateCommand,
                                             String computedDateCommand,
                                             boolean comparingNanos) {
        CalendarFactory.setBusinessDate("now",
                                        startingDateCommand);
        ZonedDateTime expectedLDT = CalendarFactory.asZoned("now",
                                                                  expectedDateCommand);

        CalendarFactory.setBusinessDate("now",
                                        startingDateCommand);
        ZonedDateTime computedLDT = CalendarFactory.asZoned("now",
                                                                  computedDateCommand);

        Assertions.assertEquals(expectedLDT.getYear(), computedLDT.getYear());
        Assertions.assertEquals(expectedLDT.getMonthValue(), computedLDT.getMonthValue());
        Assertions.assertEquals(expectedLDT.getDayOfMonth(), computedLDT.getDayOfMonth());
        Assertions.assertEquals(expectedLDT.getHour(), computedLDT.getHour());
        Assertions.assertEquals(expectedLDT.getMinute(), computedLDT.getMinute());
        Assertions.assertEquals(expectedLDT.getSecond(), computedLDT.getSecond());

        if (comparingNanos) {
            Assertions.assertEquals(expectedLDT.getNano(), computedLDT.getNano());
        }
    }
}
