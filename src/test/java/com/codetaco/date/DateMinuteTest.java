package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateMinuteTest {

    @Test
    public void add_invalidBeginning() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                        "",
                                                        "+bminute");
            Assertions.fail("expected an exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD",
                                    e.getMessage());
        }
    }

    @Test
    public void add_minute() {
        CalendarFactoryHelper.startExpectedComputed("=bt =0hours =10minutes",
                                                    "=11min",
                                                    "+1minute");
    }

    @Test
    public void atBeforeCurrentTimeResultsInPastTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =30min",
                                                    "-15minutes",
                                                    "=15minutes");
    }

    @Test
    public void atEndWhenAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=et =13hours",
                                                    "",
                                                    "=eminute");
    }

    @Test
    public void atEndWhenNotAtEnd() {
        CalendarFactoryHelper.startExpectedComputed("=bt =13hours =4min",
                                                    "=et =13hour =4minutes",
                                                    "=eminutes");
    }

    @Test
    public void beginningAt() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min =1sec =2ms",
                                                    "=bt =1hours =10min =0sec =0ms",
                                                    "=bminute");
    }

    @Test
    public void beginningNext() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min =1sec =1ms",
                                                    "=bt =1hours =11min =0s =0n",
                                                    ">bmin",
                                                    true);
    }

    @Test
    public void beginningNextOrThis_beginning() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=bt =1hours =10min",
                                                    ">=bmin");
    }

    @Test
    public void beginningNextOrThis_notBeginning() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min =15sec =30ms",
                                                    "=11min =0s =0n",
                                                    ">=bmin");
    }

    @Test
    public void beginningPrev_after() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min =1sec",
                                                    "=bt =1hours =10min",
                                                    "<bmin");
    }

    @Test
    public void beginningPrev_beginning() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=bt =1hours =9min",
                                                    "<bmin");
    }

    @Test
    public void beginningPrevOrThis_after() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min =1sec =2ms",
                                                    "=bt =1hours =10min =0s =0n",
                                                    "<=bmin",
                                                    true);
    }

    @Test
    public void beginningPrevOrThis_beginning() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=bt =1hours =10min",
                                                    "<=bmin");
    }

    @Test
    public void endingNext_ending() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min",
                                                    "=et =1hours =11min",
                                                    ">emin");
    }

    @Test
    public void endingNext_notEnding() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=et =1hours =10min",
                                                    ">emin");
    }

    @Test
    public void endingNextOrThis_ending() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min",
                                                    "",
                                                    ">=emin");
    }

    @Test
    public void endingNextOrThis_notEnding() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min =1sec",
                                                    "=et =1hours =9min",
                                                    "<=emin");
    }

    @Test
    public void endingPrev_ending() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min",
                                                    "=et =1hours =9min",
                                                    "<emin");
    }

    @Test
    public void endingPrev_notEnding() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=et =1hours =9min",
                                                    "<emin");
    }

    @Test
    public void endingPrevOrThis_ending() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min",
                                                    "",
                                                    "<=emin");
    }

    @Test
    public void endingPrevOrThis_notEnding() {
        CalendarFactoryHelper.startExpectedComputed("=et =1hours =10min =1sec",
                                                    "=et =1hours =9min",
                                                    "<=emin");
    }

    @Test
    public void greaterBeforeCurrentTimeResultsInFutureTime() {
        CalendarFactoryHelper.startExpectedComputed("=bt =12hours =30min",
                                                    "=13hours =4min",
                                                    ">4min");
    }

    @Test
    public void nextOrThisMinute_after() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "=11min",
                                                    ">=11minute");
    }

    @Test
    public void nextOrThisMinute_before() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "=2hour =9min",
                                                    ">=9minute");
    }

    @Test
    public void nextOrThisMinute_same() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10minutes",
                                                    "",
                                                    ">=10minute");
    }

    @Test
    public void prev_after() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=0hour =11min",
                                                    "<11minute");
    }

    @Test
    public void prev_before() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=9minute",
                                                    "<9minute");
    }

    @Test
    public void prev_same() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=0hour",
                                                    "<10minute");
    }

    @Test
    public void prevOrThis_after() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=0hours =11min",
                                                    "<=11minute");
    }

    @Test
    public void prevOrThis_before() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "=9min",
                                                    "<=9minute");
    }

    @Test
    public void prevOrThis_same() {
        CalendarFactoryHelper.startExpectedComputed("=bt =1hours =10min",
                                                    "",
                                                    "<=10minute");
    }

}
