package com.codetaco.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * = QTY is covered by the expected test case computations so there are not
 * specific tests for that.
 */
public class AllPossibleModificationTest {

    @Test
    public void atBegDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0h =0mi =0s =0n",
                                                    "=bd",
                                                    true);
    }

    @Test
    public void atBegDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4d",
                                                    "=bdow",
                                                    true);
    }

    @Test
    public void atBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0mi =0s =0n",
                                                    "=bh",
                                                    true);
    }

    @Test
    public void atBegMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0s =0n",
                                                    "=bi",
                                                    true);
    }

    @Test
    public void atBegMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =1d =0h =0mi =0s =0n",
                                                    "=bm",
                                                    true);
    }

    @Test
    public void atBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "=bms",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: AT", e.getMessage());
        }
    }

    @Test
    public void atBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "=bn",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: AT", e.getMessage());
        }
    }

    @Test
    public void atBegSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0n",
                                                    "=bs",
                                                    true);
    }

    @Test
    public void atBegYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1m =1d =0h =0mi =0s =0n",
                                                    "=by",
                                                    true);
    }

    @Test
    public void atEndDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=23h =59mi =59s =999999999n",
                                                    "=ed",
                                                    true);
    }

    @Test
    public void atEndDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    "=edow",
                                                    true);
    }

    @Test
    public void atEndHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =59i =59s =999999999n",
                                                    "=59mi =59s =999999999n",
                                                    "=eh",
                                                    true);
    }

    @Test
    public void atEndMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=59s =999999999n",
                                                    "=ei",
                                                    true);
    }

    @Test
    public void atEndMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =30d =23h =59mi =59s =999999999n",
                                                    "=em",
                                                    true);
    }

    @Test
    public void atEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "=ems",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: AT", e.getMessage());
        }
    }

    @Test
    public void atEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "=en",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: AT", e.getMessage());
        }
    }

    @Test
    public void atEndSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=999999999n",
                                                    "=es",
                                                    true);
    }

    @Test
    public void atEndYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12m =31d =23h =59mi =59s =999999999n",
                                                    "=ey",
                                                    true);
    }

    @Test
    public void atQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5d",
                                                    "=2dow",
                                                    true);
    }

    @Test
    public void minusBegDay() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bd",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegDOW() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bdow",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegHour() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bh",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegMin() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bi",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegMonth() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bm",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bms",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bn",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegSec() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-bs",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusBegYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-by",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndDay() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-ed",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndDOW() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-edow",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndHour() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-eh",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndMin() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-ei",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndMonth() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-em",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-ems",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-en",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndSec() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-es",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusEndYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "-ey",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: SUBTRACT", e.getMessage());
        }
    }

    @Test
    public void minusQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=8d",
                                                    "-1d",
                                                    true);
    }

    @Test
    public void minusQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=8d",
                                                    "-1dow",
                                                    true);
    }

    @Test
    public void minusQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n", "=12h", "-1h", true);
    }

    @Test
    public void minusQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11i",
                                                    "-1i",
                                                    true);
    }

    @Test
    public void minusQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=3m",
                                                    "-1m",
                                                    true);
    }

    @Test
    public void minusQtyMs() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s =999000011n",
                                                    "-1ms",
                                                    true);
    }

    @Test
    public void minusQtyNano() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10n",
                                                    "-1n",
                                                    true);
    }

    @Test
    public void minusQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s",
                                                    "-1s",
                                                    true);
    }

    @Test
    public void minusQtyYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1959y",
                                                    "-1y",
                                                    true);
    }

    @Test
    public void nextBegDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =0h =0mi =0s =0n",
                                                    ">bd",
                                                    true);
    }

    @Test
    public void nextBegDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11d =0h =0mi =0s =0n",
                                                    ">bdow",
                                                    true);
    }

    @Test
    public void nextBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=14h =0mi =0s =0n",
                                                    ">bh",
                                                    true);
    }

    @Test
    public void nextBegMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13mi =0s =0n",
                                                    ">bi",
                                                    true);
    }

    @Test
    public void nextBegMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5m =1d =0h =0mi =0s =0n",
                                                    ">bm",
                                                    true);
    }

    @Test
    public void nextBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        ">bms",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        ">bn",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextBegSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11s =0n",
                                                    ">bs",
                                                    true);
    }

    @Test
    public void nextBegYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1961y =1m =1d =0h =0mi =0s =0n",
                                                    ">by",
                                                    true);
    }

    @Test
    public void nextEndDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    ">ed",
                                                    true);
    }

    @Test
    public void nextEndDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    ">edow",
                                                    true);
    }

    @Test
    public void nextEndHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13h =59mi =59s =999999999n",
                                                    ">eh",
                                                    true);
    }

    @Test
    public void nextEndMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12mi =59s =999999999n",
                                                    ">ei",
                                                    true);
    }

    @Test
    public void nextEndMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =30d =23h =59mi =59s =999999999n",
                                                    ">em",
                                                    true);
    }

    @Test
    public void nextEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        ">ems",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        ">en",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextEndSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10s =999999999n",
                                                    ">es",
                                                    true);
    }

    @Test
    public void nextEndYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1960y =12m =31d =23h =59mi =59s =999999999n",
                                                    ">ey",
                                                    true);
    }

    @Test
    public void nextOrThisBegDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =0h =0mi =0s =0n",
                                                    ">=bd",
                                                    true);
    }

    @Test
    public void nextOrThisBegDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11d =0h =0mi =0s =0n",
                                                    ">=bdow",
                                                    true);
    }

    @Test
    public void nextOrThisBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=14h =0mi =0s =0n",
                                                    ">=bh",
                                                    true);
    }

    @Test
    public void nextOrThisBegMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13mi =0s =0n",
                                                    ">=bi",
                                                    true);
    }

    @Test
    public void nextOrThisBegMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5m =1d =0h =0mi =0s =0n",
                                                    ">=bm",
                                                    true);
    }

    @Test
    public void nextOrThisBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        ">=bms",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXTORTHIS", e.getMessage());
        }
    }

    @Test
    public void nextOrThisBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        ">=bn",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXTORTHIS", e.getMessage());
        }
    }

    @Test
    public void nextOrThisBegSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11s =0n",
                                                    ">=bs",
                                                    true);
    }

    @Test
    public void nextOrThisBegYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1961y =1m =1d =0h =0mi =0s =0n",
                                                    ">=by",
                                                    true);
    }

    @Test
    public void nextOrThisEndDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=23h =59mi =59s =999999999n",
                                                    ">=ed",
                                                    true);
    }

    @Test
    public void nextOrThisEndDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    ">=edow",
                                                    true);
    }

    @Test
    public void nextOrThisEndHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13h =59mi =59s =999999999n",
                                                    ">=eh",
                                                    true);
    }

    @Test
    public void nextOrThisEndMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12mi =59s =999999999n",
                                                    ">=ei",
                                                    true);
    }

    @Test
    public void nextOrThisEndMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =30d =23h =59mi =59s =999999999n",
                                                    ">=em",
                                                    true);
    }

    @Test
    public void nextOrThisEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        ">=ems",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXTORTHIS", e.getMessage());
        }
    }

    @Test
    public void nextOrThisEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        ">=en",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXTORTHIS", e.getMessage());
        }
    }

    @Test
    public void nextOrThisEndSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10s =999999999n",
                                                    ">=es",
                                                    true);
    }

    @Test
    public void nextOrThisEndYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1960y =12m =31d =23h =59mi =59s =999999999n",
                                                    ">=ey",
                                                    true);
    }

    @Test
    public void nextOrThisQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5m =1d =0h =0mi =0s =0n",
                                                    ">=1d",
                                                    true);
    }

    @Test
    public void nextOrThisQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11d =0h =0mi =0s =0n",
                                                    ">=1dow",
                                                    true);
    }

    @Test
    public void nextOrThisQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =1h =0mi =0s =0n",
                                                    ">=1h",
                                                    true);
    }

    @Test
    public void nextOrThisQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=14h =1mi =0s =0n",
                                                    ">=1i",
                                                    true);
    }

    @Test
    public void nextOrThisQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1961y =3m =1d =0h =0mi =0s =0n",
                                                    ">=3m",
                                                    true);
    }

    @Test
    public void nextOrThisQtyMs() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10s =1000000n",
                                                    ">=1ms",
                                                    true);
    }

    @Test
    public void nextOrThisQtyNano() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11s =1n",
                                                    ">=1n",
                                                    true);
    }

    @Test
    public void nextOrThisQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13i =1s =0n",
                                                    ">=1s",
                                                    true);
    }

    @Test
    public void nextOrThisQtyYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1961y =1m =1d =0h =0mi =0s =0n",
                                                        ">=1962y",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: NEXTORTHIS", e.getMessage());
        }
    }

    @Test
    public void nextQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5m =1d =0h =0mi =0s =0n",
                                                    ">1d",
                                                    true);
    }

    @Test
    public void nextQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11d =0h =0mi =0s =0n",
                                                    ">1dow",
                                                    true);
    }

    @Test
    public void nextQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =1h =0mi =0s =0n",
                                                    ">1h",
                                                    true);
    }

    @Test
    public void nextQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=14h =1mi =0s =0n",
                                                    ">1i",
                                                    true);
    }

    @Test
    public void nextQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1961y =2m =1d =0h =0mi =0s =0n",
                                                    ">2m",
                                                    true);
    }

    @Test
    public void nextQtyMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000000n",
                                                        ">1ms",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextQtyNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=11s =1n",
                                                        ">1n",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void nextQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13i =1s =0n",
                                                    ">1s",
                                                    true);
    }

    @Test
    public void nextQtyYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1961y =1m =1d =0h =0mi =0s =0n",
                                                        ">1962y",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void plusBegDay() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bd",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegDOW() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bdow",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegHour() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bh",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegMin() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bi",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegMonth() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bm",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bms",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bn",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegSec() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+bs",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusBegYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+by",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndDay() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+ed",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndDOW() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+edow",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndHour() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+eh",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndMin() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+ei",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndMonth() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+em",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+ems",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+en",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndSec() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+es",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusEndYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "",
                                                        "+ey",
                                                        true);
            Assertions.fail("expected exception");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: ADD", e.getMessage());
        }
    }

    @Test
    public void plusQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d",
                                                    "+1d",
                                                    true);
    }

    @Test
    public void plusQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d",
                                                    "+1dow",
                                                    true);
    }

    @Test
    public void plusQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=14h",
                                                    "+1h",
                                                    true);
    }

    @Test
    public void plusQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13i",
                                                    "+1i",
                                                    true);
    }

    @Test
    public void plusQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=5m",
                                                    "+1m",
                                                    true);
    }

    @Test
    public void plusQtyMs() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1000011n",
                                                    "+1ms",
                                                    true);
    }

    @Test
    public void plusQtyNano() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12n",
                                                    "+1n",
                                                    true);
    }

    @Test
    public void plusQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11s",
                                                    "+1s",
                                                    true);
    }

    @Test
    public void plusQtyYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1961y",
                                                    "+1y",
                                                    true);
    }

    @Test
    public void prevBegDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0h =0mi =0s =0n",
                                                    "<bd",
                                                    true);
    }

    @Test
    public void prevBegDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4d =23h =59mi =59s =999999999n",
                                                    "<bdow",
                                                    true);
    }

    @Test
    public void prevBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13h =0mi =0s =0n",
                                                    "<bh",
                                                    true);
    }

    @Test
    public void prevBegMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12mi =0s =0n",
                                                    "<bi",
                                                    true);
    }

    @Test
    public void prevBegMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =1d =0h =0mi =0s =0n",
                                                    "<bm",
                                                    true);
    }

    @Test
    public void prevBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "<bms",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREV", e.getMessage());
        }
    }

    @Test
    public void prevBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "<bn",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREV", e.getMessage());
        }
    }

    @Test
    public void prevBegSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10s =0n",
                                                    "<bs",
                                                    true);
    }

    @Test
    public void prevBegYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1960y =1m =1d =0h =0mi =0s =0n",
                                                    "<by",
                                                    true);
    }

    @Test
    public void prevEndDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=8d =23h =59mi =59s =999999999n",
                                                    "<ed",
                                                    true);
    }

    @Test
    public void prevEndDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    "<edow",
                                                    true);
    }

    @Test
    public void prevEndHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12h =59mi =59s =999999999n",
                                                    "<eh",
                                                    true);
    }

    @Test
    public void prevEndMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11mi =59s =999999999n",
                                                    "<ei",
                                                    true);
    }

    @Test
    public void prevEndMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=3m =31d =23h =59mi =59s =999999999n",
                                                    "<em",
                                                    true);
    }

    @Test
    public void prevEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "<ems",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREV", e.getMessage());
        }
    }

    @Test
    public void prevEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "<en",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREV", e.getMessage());
        }
    }

    @Test
    public void prevEndSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s =999999999n",
                                                    "<es",
                                                    true);
    }

    @Test
    public void prevEndYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1959y =12m =31d =23h =59mi =59s =999999999n",
                                                    "<ey",
                                                    true);
    }

    @Test
    public void prevOrThisBegDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=0h =0mi =0s =0n",
                                                    "<=bd",
                                                    true);
    }

    @Test
    public void prevOrThisBegDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4d =23h =59mi =59s =999999999n",
                                                    "<=bdow",
                                                    true);
    }

    @Test
    public void prevOrThisBegHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=13h =0mi =0s =0n",
                                                    "<=bh",
                                                    true);
    }

    @Test
    public void prevOrThisBegMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12mi =0s =0n",
                                                    "<=bi",
                                                    true);
    }

    @Test
    public void prevOrThisBegMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4m =1d =0h =0mi =0s =0n",
                                                    "<=bm",
                                                    true);
    }

    @Test
    public void prevOrThisBegMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "<=bms",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREVORTHIS", e.getMessage());
        }
    }

    @Test
    public void prevOrThisBegNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "<=bn",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREVORTHIS", e.getMessage());
        }
    }

    @Test
    public void prevOrThisBegSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10s =0n",
                                                    "<=bs",
                                                    true);
    }

    @Test
    public void prevOrThisBegYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1960y =1m =1d =0h =0mi =0s =0n",
                                                    "<=by",
                                                    true);
    }

    @Test
    public void prevOrThisEndDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=8d =23h =59mi =59s =999999999n",
                                                    "<=ed",
                                                    true);
    }

    @Test
    public void prevOrThisEndDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=10d =23h =59mi =59s =999999999n",
                                                    "<=edow",
                                                    true);
    }

    @Test
    public void prevOrThisEndHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12h =59mi =59s =999999999n",
                                                    "<=eh",
                                                    true);
    }

    @Test
    public void prevOrThisEndMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=11mi =59s =999999999n",
                                                    "<=ei",
                                                    true);
    }

    @Test
    public void prevOrThisEndMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=3m =31d =23h =59mi =59s =999999999n",
                                                    "<=em",
                                                    true);
    }

    @Test
    public void prevOrThisEndMs() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1000011n",
                                                        "<=ems",
                                                        true);

        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREVORTHIS", e.getMessage());
        }
    }

    @Test
    public void prevOrThisEndNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=12n",
                                                        "<=en",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: PREVORTHIS", e.getMessage());
        }
    }

    @Test
    public void prevOrThisEndSecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s =999999999n",
                                                    "<=es",
                                                    true);
    }

    @Test
    public void prevOrThisEndYear() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1959y =12m =31d =23h =59mi =59s =999999999n",
                                                    "<=ey",
                                                    true);
    }

    @Test
    public void prevOrThisQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1d =23h =59mi =59s =999999999n",
                                                    "<=1d",
                                                    true);
    }

    @Test
    public void prevOrThisQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4d =23h =59mi =59s =999999999n",
                                                    "<=1dow",
                                                    true);
    }

    @Test
    public void prevOrThisQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9d =1h =0mi =0s =0n",
                                                    "<=1h",
                                                    true);
    }

    @Test
    public void prevOrThisQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1mi =0s =0n",
                                                    "<=1i",
                                                    true);
    }

    @Test
    public void prevOrThisQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=3m =31d =23h =59mi =59s =999999999n",
                                                    "<=3m",
                                                    true);
    }

    @Test
    public void prevOrThisQtyMs() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s =1000000n",
                                                    "<=1ms",
                                                    true);
    }

    @Test
    public void prevOrThisQtyNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=10s =1n",
                                                        "<=1n",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void prevOrThisQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12i =1s =999999999n",
                                                    "<=1s",
                                                    true);
    }

    @Test
    public void prevOrThisQtyYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1961y =1m =1d =0h =0mi =0s =0n",
                                                        "<=1962y",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: PREVORTHIS", e.getMessage());
        }
    }

    @Test
    public void prevQtyDay() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1d =23h =59mi =59s =999999999n",
                                                    "<1d",
                                                    true);
    }

    @Test
    public void prevQtyDOW() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=4d =23h =59mi =59s =999999999n",
                                                    "<1dow",
                                                    true);
    }

    @Test
    public void prevQtyHour() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9d =1h =0mi =0s =0n",
                                                    "<1h",
                                                    true);
    }

    @Test
    public void prevQtyMinute() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=1mi =0s =0n",
                                                    "<1i",
                                                    true);
    }

    @Test
    public void prevQtyMonth() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=3m =31d =23h =59mi =59s =999999999n",
                                                    "<3m",
                                                    true);
    }

    @Test
    public void prevQtyMs() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=9s =1000000n",
                                                    "<1ms",
                                                    true);
    }

    @Test
    public void prevQtyNano() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=10s =1n",
                                                        "<1n",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid quantity in data adjustment: NEXT", e.getMessage());
        }
    }

    @Test
    public void prevQtySecond() {
        CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                    "=12i =1s =999999999n",
                                                    "<1s",
                                                    true);
    }

    @Test
    public void prevQtyYear() {
        try {
            CalendarFactoryHelper.startExpectedComputed("=1960y =04M =09d =13h =12i =10s =11n",
                                                        "=1961y =1m =1d =0h =0mi =0s =0n",
                                                        "<1962y",
                                                        true);
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid direction in data adjustment: PREV", e.getMessage());
        }
    }
}
