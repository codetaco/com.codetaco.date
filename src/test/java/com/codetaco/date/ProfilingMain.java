package com.codetaco.date;

import com.codetaco.date.impl.TemporalHelper;

import java.util.stream.IntStream;

public class ProfilingMain {

    public static void main(String[] args) {

        IntStream.range(1, 10001).forEach(
          (i) ->
          {
              try {
                  TemporalHelper.parseWithPredefinedParsers("Apr 9, 1960");
              } catch (Exception e) {
                  //
              }
          });
        TemporalHelper.usageReport().forEach(System.out::println);
    }
}
