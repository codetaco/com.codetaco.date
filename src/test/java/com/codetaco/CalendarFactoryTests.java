package com.codetaco;

import com.codetaco.date.CalendarFactory;
import com.codetaco.date.CalendarFactoryException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

public class CalendarFactoryTests {

    private static final ZonedDateTime staticZoned;
    private static final Date staticDate;
    private static final LocalDate staticLocalDate;
    private static final LocalTime staticLocalTime;
    private static final LocalDateTime staticLocalDateTime;
    private static final Calendar staticCalendar;
    private static final String staticString;
    private static final long staticLong;

    static {
        CalendarFactory.setBusinessDate("2018-10-20T12:38:15.123Z");
        staticZoned = CalendarFactory.asZoned("now");
        staticDate = CalendarFactory.asDate("now");
        staticLocalDate = CalendarFactory.asLocalDate("now");
        staticLocalTime = CalendarFactory.asLocalTime("now");
        staticLocalDateTime = CalendarFactory.asLocalDateTime("now");
        staticCalendar = CalendarFactory.asCalendar("now");
        staticString = CalendarFactory.asJSON("now");
        staticLong = CalendarFactory.asLong("now");
    }

    @Test
    public void asZonedDate() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticDate, "-1d")));
    }

    @Test
    public void asZonedLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticLocalDate, "-1d")));
    }

    @Test
    public void asZonedLocalTime() {
        Assertions.assertEquals("-999999999-01-01T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticLocalTime, "-0h")));
    }

    @Test
    public void asZonedLocalDateTime() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asZonedCalendar() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticCalendar, "-1d")));
    }

    @Test
    public void asZonedString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticString, "-1d")));
    }

    @Test
    public void asZonedLong() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticLong, "-1d")));
    }

    @Test
    public void asZonedZoned() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asZoned(staticZoned, "-1d")));
    }


    @Test
    public void asCalendarDate() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticDate, "-1d")));
    }

    @Test
    public void asCalendarLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticLocalDate, "-1d")));
    }

    @Test
    public void asCalendarLocalTime() {

        try {
            CalendarFactory.asCalendar(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a calendar",
                                e.getMessage());
        }
    }

    @Test
    public void asCalendarLocalDateTime() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asCalendarCalendar() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticCalendar, "-1d")));
    }

    @Test
    public void asCalendarString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticString, "-1d")));
    }

    @Test
    public void asCalendarLong() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticLong, "-1d")));
    }

    @Test
    public void asCalendarZoned() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asCalendar(staticZoned, "-1d")));
    }

    @Test
    public void asLocalDateTimeDate() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticDate, "-1d")));
    }

    @Test
    public void asLocalDateTimeLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticLocalDate, "-1d")));
    }

    @Test
    public void asLocalDateTimeLocalTime() {
        try {
            CalendarFactory.asLocalDateTime(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a local date time",
                                e.getMessage());
        }
    }

    @Test
    public void asLocalDateTimeLocalDateTime() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asLocalDateTimeCalendar() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticCalendar, "-1d")));
    }

    @Test
    public void asLocalDateTimeString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticString, "-1d")));
    }

    @Test
    public void asLocalDateTimeLong() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticLong, "-1d")));
    }

    @Test
    public void asLocalDateTimeZoned() {
        Assertions.assertEquals("2018-10-19T12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalDateTime(staticZoned, "-1d")));
    }

    @Test
    public void asLocalDateDate() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticDate, "-1d")));
    }

    @Test
    public void asLocalDateLocalDate() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticLocalDate, "-1d")));
    }

    @Test
    public void asLocalDateLocalTime() {
        try {
            CalendarFactory.asLocalDate(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a local date",
                                e.getMessage());
        }
    }

    @Test
    public void asLocalDateLocalDateTime() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asLocalDateCalendar() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticCalendar, "-1d")));
    }

    @Test
    public void asLocalDateString() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticString, "-1d")));
    }

    @Test
    public void asLocalDateLong() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticLong, "-1d")));
    }

    @Test
    public void asLocalDateZoned() {
        Assertions.assertEquals("2018-10-19", CalendarFactory.asJSON(
          CalendarFactory.asLocalDate(staticZoned, "-1d")));
    }

    @Test
    public void asLocalTimeDate() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticDate, "-1d")));
    }

    @Test
    public void asLocalTimeLocalDate() {
        Assertions.assertEquals("00:00:00", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticLocalDate, "-1d")));
    }

    @Test
    public void asLocalTimeLocalTime() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticLocalTime, "-0h")));
    }

    @Test
    public void asLocalTimeLocalDateTime() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asLocalTimeCalendar() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticCalendar, "-1d")));
    }

    @Test
    public void asLocalTimeString() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticString, "-1d")));
    }

    @Test
    public void asLocalTimeLong() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticLong, "-1d")));
    }

    @Test
    public void asLocalTimeZoned() {
        Assertions.assertEquals("12:38:15.123", CalendarFactory.asJSON(
          CalendarFactory.asLocalTime(staticZoned, "-1d")));
    }

    @Test
    public void asDateDate() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticDate, "-1d")));
    }

    @Test
    public void asDateLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticLocalDate, "-1d")));
    }

    @Test
    public void asDateLocalTime() {
        try {
            CalendarFactory.asDate(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a date",
                                e.getMessage());
        }
    }

    @Test
    public void asDateLocalDateTime() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asDateCalendar() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticCalendar, "-1d")));
    }

    @Test
    public void asDateString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticString, "-1d")));
    }

    @Test
    public void asDateLong() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticLong, "-1d")));
    }

    @Test
    public void asDateZoned() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asDate(staticZoned, "-1d")));
    }

    @Test
    public void asDateLongDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticDate, "-1d")));
    }

    @Test
    public void asDateLongLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticLocalDate, "-1d")));
    }

    @Test
    public void asDateLongLocalTime() {
        try {
            CalendarFactory.asDateLong(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a date long",
                                e.getMessage());
        }
    }

    @Test
    public void asDateLongLocalDateTime() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asDateLongCalendar() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticCalendar, "-1d")));
    }

    @Test
    public void asDateLongString() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticString, "-1d")));
    }

    @Test
    public void asDateLongLong() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticLong, "-1d")));
    }

    @Test
    public void asDateLongZoned() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asDateLong(staticZoned, "-1d")));
    }

    @Test
    public void asLongDate() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticDate, "-1d")));
    }

    @Test
    public void asLongLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticLocalDate, "-1d")));
    }

    @Test
    public void asLongLocalTime() {
        try {
            CalendarFactory.asLong(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a long",
                                e.getMessage());
        }
    }

    @Test
    public void asLongLocalDateTime() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticLocalDateTime, "-1d")));
    }

    @Test
    public void asLongCalendar() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticCalendar, "-1d")));
    }

    @Test
    public void asLongString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticString, "-1d")));
    }

    @Test
    public void asLongLong() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticLong, "-1d")));
    }

    @Test
    public void asLongZoned() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z", CalendarFactory.asJSON(
          CalendarFactory.asLong(staticZoned, "-1d")));
    }

    @Test
    public void asFormulaDate() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticDate, "-1d"));
    }

    @Test
    public void asFormulaLocalDate() {
        Assertions.assertEquals("=2018year =10month =19day =0hour =0minute =0second =0nanosecond",
                            CalendarFactory.asFormula(staticLocalDate, "-1d"));
    }

    @Test
    public void asFormulaLocalTime() {
        try {
            CalendarFactory.asFormula(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to convert a time to a formula",
                                e.getMessage());
        }
    }

    @Test
    public void asFormulaLocalDateTime() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticLocalDateTime, "-1d"));
    }

    @Test
    public void asFormulaCalendar() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticCalendar, "-1d"));
    }

    @Test
    public void asFormulaString() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticString, "-1d"));
    }

    @Test
    public void asFormulaLong() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticLong, "-1d"));
    }

    @Test
    public void asFormulaZoned() {
        Assertions.assertEquals("=2018year =10month =19day =12hour =38minute =15second =123000000nanosecond",
                            CalendarFactory.asFormula(staticZoned, "-1d"));
    }


    @Test
    public void debugging() {
        CalendarFactory.setInDebug(false);
    }

    @Test
    public void asJsonString() {
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(staticString, "-1d"));
    }

    @Test
    public void noTimeDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticDate, "-1d")));
    }

    @Test
    public void noTimeLocalDate() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticLocalDate, "-1d")));
    }

    @Test
    public void noTimeLocalTime() {
        try {
            CalendarFactory.noTime(staticLocalTime);
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to remove time from a time",
                                e.getMessage());
        }
    }

    @Test
    public void noTimeLocalDateTime() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticLocalDateTime, "-1d")));
    }

    @Test
    public void noTimeCalendar() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticCalendar, "-1d")));
    }

    @Test
    public void noTimeString() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticString, "-1d")));
    }

    @Test
    public void noTimeLong() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticLong, "-1d")));
    }

    @Test
    public void noTimeZoned() {
        Assertions.assertEquals("2018-10-19T00:00:00Z", CalendarFactory.asJSON(
          CalendarFactory.noTime(staticZoned, "-1d")));
    }

    @Test
    public void setBusinessDateDate() {
        CalendarFactory.setBusinessDate(staticDate, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateLocalDate() {
        CalendarFactory.setBusinessDate(staticLocalDate, "-1d");
        Assertions.assertEquals("2018-10-19T00:00:00Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateLocalTime() {
        try {
            CalendarFactory.setBusinessDate(staticLocalTime, "-1d");
            Assertions.fail("expected failure");
        } catch (CalendarFactoryException e) {
            Assertions.assertEquals("invalid to set a business date to a time",
                                e.getMessage());
        }
    }

    @Test
    public void setBusinessDateLocalDateTime() {
        CalendarFactory.setBusinessDate(staticLocalDateTime, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateCalendar() {
        CalendarFactory.setBusinessDate(staticCalendar, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateString() {
        CalendarFactory.setBusinessDate(staticString, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateLong() {
        CalendarFactory.setBusinessDate(staticLong, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }

    @Test
    public void setBusinessDateZoned() {
        CalendarFactory.setBusinessDate(staticZoned, "-1d");
        Assertions.assertEquals("2018-10-19T12:38:15.123Z",
                            CalendarFactory.asJSON(CalendarFactory.asZoned("now")));
    }
}
