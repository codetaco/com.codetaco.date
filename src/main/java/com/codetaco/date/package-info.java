/**
 <p>
 This package contains the public API into the CalendarFactory class.  All methods on the class are static so no
 instances need to be created.
 </p>
 <p>
 Almost everything is accessed in a very simple API to convert a date / time. from one format into another - and in the
 process, optionally apply adjustments to the resulting date / time.
 </p>
 <h3>Build instructions</h3>
 <ul>
 <li>version number - update the version number in these files
 <ul>
    <li>pom.xml</li><li>package-info.java</li><li>docs/conf.py</li>
 </ul>
 <li>submit a pull-request to master</li>
 <li>after the bitbucket pipeline is complete then got to bintray and push to maven central</li>
 </ul>

 <p><a href="http://date.readthedocs.io">Read the documentation</a></p>

 @author cdegreef
 @since 5.0.4 */
package com.codetaco.date;
