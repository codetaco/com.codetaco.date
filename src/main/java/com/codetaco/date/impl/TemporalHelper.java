package com.codetaco.date.impl;

import com.codetaco.date.impl.helper.ITemporalHelperImpl;
import com.codetaco.date.impl.helper.TemporalHelperAbstractImpl;
import com.codetaco.date.impl.helper.TemporalHelperUSImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class TemporalHelper {
    static ITemporalHelperImpl delegate;

    static ITemporalHelperImpl getDelegate() {
        if (delegate == null) {
            delegate = new TemporalHelperUSImpl();
        }
        return delegate;
    }

    static public List<TemporalHelperAbstractImpl.UsageReport> usageReport() {
        return getDelegate().usageReport();
    }

    static public DateTimeFormatter getOutputDF() {
        return getDelegate().getOutputDF();
    }

    static public DateTimeFormatter getOutputDTF() {
        return getDelegate().getOutputDTF();
    }

    static public SimpleDateFormat getOutputSDF() {
        return getDelegate().getOutputSDF();
    }

    static public DateTimeFormatter getOutputTF() {
        return getDelegate().getOutputTF();
    }

    static public ZonedDateTime parseWithPredefinedParsers(String valueStr)
      throws ParseException {
        return getDelegate().parseWithPredefinedParsers(valueStr,
                                                        null,
                                                        ZoneOffset.UTC);
    }

    static public ZonedDateTime parseWithPredefinedParsers(String valueStr,
                                                           ZonedDateTime overrideSystemDateTime)
      throws ParseException {
        return getDelegate().parseWithPredefinedParsers(valueStr,
                                                        overrideSystemDateTime,
                                                        ZoneOffset.UTC);
    }

    static public ZonedDateTime parseWithPredefinedParsers(String valueStr,
                                                           ZonedDateTime overrideSystemDateTime,
                                                           ZoneId defaultZoneId)
      throws ParseException {
        return getDelegate().parseWithPredefinedParsers(valueStr,
                                                        overrideSystemDateTime,
                                                        defaultZoneId);
    }

    static public ITemporalHelperImpl setDelegate(ITemporalHelperImpl newImpl) {
        ITemporalHelperImpl previousImpl = delegate;
        delegate = newImpl;
        return previousImpl;
    }

    static public String allFormats() {
        return getDelegate().toString();
    }
}
