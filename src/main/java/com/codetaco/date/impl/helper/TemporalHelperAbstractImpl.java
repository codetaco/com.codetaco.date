package com.codetaco.date.impl.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class is thread safe.
 */
public abstract class TemporalHelperAbstractImpl implements ITemporalHelperImpl {

    private static final String specialAlgoTODAY = "today";
    private static final String specialAlgoNOW = "now";

    private int fmtMakerIndex = -1;
    private DateTimeFormatter[] predefinedFmt;
    private boolean[] predefinedFmtWithDateOnly;
    private boolean[] predefinedFmtWithTimeOnly;

    public class UsageReport {
        String pattern;
        int numberOfTimesUsed;

        UsageReport(UsageStatistics stats) {
            pattern = predefinedFmt[stats.predefinedIndex].toString();
            numberOfTimesUsed = stats.numberOfTimesUsed;
        }

        @Override
        public String toString() {
            return numberOfTimesUsed + " - " + pattern;
        }
    }

    static private class UsageStatistics {
        int predefinedIndex;
        int numberOfTimesUsed;

        UsageStatistics(int predefinedIndex) {
            this.predefinedIndex = predefinedIndex;
            numberOfTimesUsed = 0;
        }

        boolean isUsed() {
            return numberOfTimesUsed > 0;
        }

        @Override
        public String toString() {
            return "idx=" + predefinedIndex + " count=" + numberOfTimesUsed;
        }
    }

    private UsageStatistics[] usageStatistics;

    private final SimpleDateFormat outputSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private final DateTimeFormatter outputDTF = DateTimeFormatter.ISO_DATE_TIME;
    private final DateTimeFormatter outputDF = DateTimeFormatter.ISO_DATE;
    private final DateTimeFormatter outputTF = DateTimeFormatter.ISO_TIME;

    private void createFormat(DateTimeFormatterBuilder builder,
                              boolean withDatePart,
                              boolean withTimePart) {
        ++fmtMakerIndex;
        predefinedFmt[fmtMakerIndex] = buildParser(builder);
        predefinedFmtWithDateOnly[fmtMakerIndex] = withDatePart && !withTimePart;
        predefinedFmtWithTimeOnly[fmtMakerIndex] = !withDatePart && withTimePart;
    }

    private DateTimeFormatter buildParser(DateTimeFormatterBuilder builder) {
        return builder.toFormatter();
    }

    @Override
    public List<UsageReport> usageReport() {
        if (usageStatistics == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(usageStatistics)
                 .filter(Objects::nonNull)
                 .filter(UsageStatistics::isUsed)
                 .map(UsageReport::new)
                 .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Create these so that the matchers are paired with the date formats. When the matcher succeeds then the date
     * format
     * will be used.
     */
    private synchronized void createPredefinedDateFormats() {
        if (predefinedFmt != null) {
            return;
        }

        int sizeOfPredefinedArrays = 25;
        predefinedFmt = new DateTimeFormatter[sizeOfPredefinedArrays];
        predefinedFmtWithDateOnly = new boolean[sizeOfPredefinedArrays];
        predefinedFmtWithTimeOnly = new boolean[sizeOfPredefinedArrays];
        usageStatistics = new UsageStatistics[sizeOfPredefinedArrays];

        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_DATE_TIME),
                     true,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.BASIC_ISO_DATE),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_DATE),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_LOCAL_DATE),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_OFFSET_DATE),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_LOCAL_TIME),
                     false,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_OFFSET_TIME),
                     false,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_TIME),
                     false,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                     true,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
                     true,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_ZONED_DATE_TIME),
                     true,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_ORDINAL_DATE),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ISO_WEEK_DATE)
                       .parseDefaulting(ChronoField.DAY_OF_WEEK, 1),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .parseDefaulting(ChronoField.DAY_OF_WEEK, 1)
                       .parseCaseInsensitive()
                       .appendValue(IsoFields.WEEK_BASED_YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
                       .appendLiteral("-W")
                       .appendValue(IsoFields.WEEK_OF_WEEK_BASED_YEAR, 2)
                       .optionalStart()
                       .appendOffsetId(),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.RFC_1123_DATE_TIME),
                     true,
                     true);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("MMM d, yyyy")),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("d MMM, yyyy")),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("M-yyyy"))
                       .parseDefaulting(ChronoField.DAY_OF_MONTH, 1),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("yyyy-M"))
                       .parseDefaulting(ChronoField.DAY_OF_MONTH, 1),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("MM-dd-yyyy")),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .append(DateTimeFormatter.ofPattern("yyyy"))
                       .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                       .parseDefaulting(ChronoField.DAY_OF_MONTH, 1),
                     true,
                     false);
        createFormat(new DateTimeFormatterBuilder()
                       .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
                       .append(DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmm[ss][SSS][Z]")),
                     true,
                     true);

        IntStream
          .range(0, fmtMakerIndex + 1)
          .forEach(fmtIndex -> usageStatistics[fmtIndex] = new UsageStatistics(fmtIndex));
    }

    @Override
    public DateTimeFormatter getOutputDF() {
        return outputDF;
    }

    @Override
    public DateTimeFormatter getOutputDTF() {
        return outputDTF;
    }

    @Override
    public SimpleDateFormat getOutputSDF() {
        return outputSDF;
    }

    @Override
    public DateTimeFormatter getOutputTF() {
        return outputTF;
    }

    private ZonedDateTime parseSpecialDate(String pattern,
                                           ZonedDateTime overrideSystemDateTime) {
        ZonedDateTime datetimeToUse = overrideSystemDateTime;
        if (datetimeToUse == null) {
            datetimeToUse = ZonedDateTime.now();
        }

        if (specialAlgoTODAY.equalsIgnoreCase(pattern)) {
            return ZonedDateTime.of(datetimeToUse.getYear(),
                                    datetimeToUse.getMonthValue(),
                                    datetimeToUse.getDayOfMonth(),
                                    0, 0, 0, 0,
                                    datetimeToUse.getZone());
        }
        if (specialAlgoNOW.equalsIgnoreCase(pattern)) {
            return datetimeToUse;
        }
        return null;
    }

    @Override
    public ZonedDateTime parseWithPredefinedParsers(String valueStr,
                                                    ZonedDateTime overrideSystemDateTime,
                                                    ZoneId defaultZoneId) throws ParseException {
        createPredefinedDateFormats();

        ZonedDateTime localDateTime = parseSpecialDate(valueStr, overrideSystemDateTime);
        if (localDateTime != null) {
            return localDateTime;
        }

        for (int uIdx = 0; uIdx < usageStatistics.length; uIdx++) {
            UsageStatistics usages = usageStatistics[uIdx];
            if (usages == null) {
                break;
            }
            int f = usages.predefinedIndex;
            try {
                localDateTime = parse(valueStr, f, defaultZoneId);

//                System.out.println(predefinedFmt[f].toString());

                usageStatistics[uIdx].numberOfTimesUsed++;
                bubbleUp(uIdx);
                return localDateTime;
            } catch (Exception e) {
//                System.out.println(e + " " + predefinedFmt[f].toString());
            }
        }
        throw new ParseException("not in a predefined date / time format (" + valueStr + ")", 0);
    }

    private void bubbleUp(int usedPredefinedIdx) {
        UsageStatistics statsOnTheMove = usageStatistics[usedPredefinedIdx];
        int idxOnTheMove = usedPredefinedIdx;
        for (int swapTarget = usedPredefinedIdx - 1; swapTarget >= 0; swapTarget--) {
            if (usageStatistics[swapTarget].numberOfTimesUsed < statsOnTheMove.numberOfTimesUsed) {
                usageStatistics[idxOnTheMove] = usageStatistics[swapTarget];
                usageStatistics[swapTarget] = statsOnTheMove;
                idxOnTheMove = swapTarget;
            } else {
                break;
            }
        }
    }

    private ZonedDateTime parse(String valueStr,
                                int f,
                                ZoneId zoneId) {
        if (predefinedFmtWithDateOnly[f]) {
            return ZonedDateTime.of(LocalDate.parse(valueStr, predefinedFmt[f]),
                                    LocalTime.MIN,
                                    zoneId);
        }
        if (predefinedFmtWithTimeOnly[f]) {
            return ZonedDateTime.of(LocalDate.MIN,
                                    LocalTime.parse(valueStr, predefinedFmt[f]),
                                    zoneId);
        }
        try {
            return ZonedDateTime.parse(valueStr, predefinedFmt[f]);
        } catch (Exception e) {
            // try later with local date time (non-zoned)
        }
        LocalDateTime ldt = LocalDateTime.parse(valueStr, predefinedFmt[f]);
        return ZonedDateTime.of(ldt, zoneId);
    }
}
