package com.codetaco.date.impl.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public interface ITemporalHelperImpl {
    DateTimeFormatter getOutputDF();

    DateTimeFormatter getOutputDTF();

    SimpleDateFormat getOutputSDF();

    DateTimeFormatter getOutputTF();

    ZonedDateTime parseWithPredefinedParsers(String valueStr,
                                             ZonedDateTime overrideSystemDateTime,
                                             ZoneId defaultZoneId) throws ParseException;

    List<TemporalHelperAbstractImpl.UsageReport> usageReport();
}