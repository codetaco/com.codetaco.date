package com.codetaco.date.impl;

enum UnitOfMeasure {
    DAY,
    DAYOFWEEK,
    HOUR,
    MILLISECOND,
    NANOSECOND,
    MINUTE,
    MONTH,
    SECOND,
    TIME,
    YEAR
}
