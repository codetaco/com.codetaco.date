package com.codetaco.date.impl;

import java.text.ParseException;

enum AdjustmentDirection {
    ADD('+'),
    AT('='),
    NEXT('>'),
    NEXTORTHIS('>', '='),
    PREV('<'),
    PREVORTHIS('<', '='),
    SUBTRACT('-');

    static public AdjustmentDirection find(String tokenValue) throws ParseException {
        char char1 = 0x00;
        char char2 = 0x00;

        if (tokenValue.length() == 0) {
            throw new ParseException("empty token when looking for direction", 0);
        }
        if (tokenValue.length() >= 1) {
            char1 = tokenValue.charAt(0);
        }
        if (tokenValue.length() >= 2) {
            char2 = tokenValue.charAt(1);
        }

        AdjustmentDirection bestSoFar = null;
        AdjustmentDirection[] all = values();
        for (int d = 0; d < all.length; d++) {
            if (all[d].firstChar == char1) {
                if (all[d].secondChar == 0x00) {
                    if (bestSoFar == null) {
                        bestSoFar = all[d];
                    }
                } else if (all[d].secondChar == char2) {
                    return all[d];
                }
            }
        }
        if (bestSoFar != null) {
            return bestSoFar;
        }
        throw new ParseException("invalid direction: " + tokenValue, 0);
    }

    char firstChar;
    char secondChar;

    AdjustmentDirection(char char1) {
        firstChar = char1;
        secondChar = (char) 0x00;
    }

    AdjustmentDirection(char char1, char char2) {
        firstChar = char1;
        secondChar = char2;
    }

    public int size() {
        if (secondChar == 0x00) {
            return 1;
        }
        return 2;
    }
}
