package com.codetaco.date.impl;

import com.codetaco.date.CalendarFactoryException;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class CalendarFactoryImpl implements ICalendarFactory {
    ZonedDateTime overrideForNowAndToday;

    ZonedDateTime applyAdjustments(ZonedDateTime startingDate,
                                   String... adjustmentsArray) {
        try {
            ZonedDateTime modifiedDateTime = startingDate;
            if (adjustmentsArray != null) {
                List<DateAdjustment> adjustments = new ArrayList<>();
                for (String adjs : adjustmentsArray) {
                    if (adjs.trim().length() == 0) {
                        break;
                    }
                    String[] singleAdjs = adjs.split(" +");
                    for (String adj : singleAdjs) {
                        adjustments.add(new DateAdjustment(adj));
                    }
                }

                for (DateAdjustment adj : adjustments) {
                    modifiedDateTime = adj.adjust(modifiedDateTime);
                }
            }
            return modifiedDateTime;
        } catch (Exception e) {
            throw CalendarFactoryException.builder().cause(e).build();
        }
    }

    @Override
    public String asFormula(ZonedDateTime ldt) {
        char dir = AdjustmentDirection.AT.firstChar;

        StringBuilder sb = new StringBuilder();
        sb.append("" + dir).append(ldt.getYear()).append("year");
        sb.append(" " + dir).append(ldt.getMonthValue()).append("month");
        sb.append(" " + dir).append(ldt.getDayOfMonth()).append("day");
        sb.append(" " + dir).append(ldt.getHour()).append("hour");
        sb.append(" " + dir).append(ldt.getMinute()).append("minute");
        sb.append(" " + dir).append(ldt.getSecond()).append("second");
        sb.append(" " + dir).append(ldt.getNano()).append("nanosecond");
        return sb.toString();
    }

    @Override
    public ZonedDateTime modifyImpl(ZonedDateTime startingDate,
                                    String... adjustmentsArray) {
        return applyAdjustments(startingDate, adjustmentsArray);
    }

    @Override
    public ZonedDateTime noTimeImpl(ZonedDateTime startingDate) {
        return ZonedDateTime.of(
          startingDate.getYear(),
          startingDate.getMonthValue(),
          startingDate.getDayOfMonth(),
          0, 0, 0, 0, ZoneOffset.UTC);
    }

    @Override
    public ZonedDateTime nowImpl(String... adjustmentsArray) {
        return applyAdjustments(overrideIfNecessary(ZonedDateTime.now()), adjustmentsArray);
    }

    ZonedDateTime overrideIfNecessary(ZonedDateTime startingDate) {
        if (overrideForNowAndToday != null) {
            return overrideForNowAndToday;
        }
        return startingDate;
    }

    @Override
    public void setOverrideForSystemTime(ZonedDateTime overrideDateAndTime) {
        overrideForNowAndToday = overrideDateAndTime;
    }

    @Override
    public ZonedDateTime getOverrideForSystemTime() {
        return overrideForNowAndToday;
    }

    @Override
    public ZonedDateTime todayImpl(String... adjustmentsArray) {
        return applyAdjustments(noTimeImpl(overrideIfNecessary(ZonedDateTime.now())), adjustmentsArray);
    }
}
