package com.codetaco.date.impl;

enum QuantityType {
    ABSOLUTE,
    BEGINNING,
    ENDING
}
