package com.codetaco.date;

import com.codetaco.date.impl.CalendarFactoryImpl;
import com.codetaco.date.impl.ICalendarFactory;
import com.codetaco.date.impl.TemporalHelper;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * Each of the methods in this class allows for some
 * type of converstion from or to a ZonedDateTime.
 * <p>
 * Many of the methods allow for adjustments to be applied to the date as it
 * is converted.  These adjustments are an array, or a space separated string,
 * of patterns to be applied in order to the supplied date / time.
 *<p>
 * Each pattern consists of 3 parts, a direction, a quantity, and a unit of measure.
 * An example would be <code>+3Months</code> or <code>=4DayOfWeek</code>.  There
 * can be no whitespace within a pattern.
 * <p>
 * For specifics and examples see <a href="http://date.readthedocs.org"> the documentation</a>.
 * <h3>Direction</h3>
 * A direction can be one of "+", "-", or "=".  Relative (or sliding) directions
 * can be one of "&gt;", "&gt;=, "&lt;", or "&lt;=".
 * <h3>Quantity</h3>
 * The quantity can be one of an integer, "B", or "E".
 * <h3>Unit of measure</h3>
 * The unit of measure can be one of "Year", "Month", "Day", "DayOfWeek",
 * "Hour", "Minute", "Second", "Millisecond", "NanoSecond", or "Time".
 * <p>
 * Some of the methods convert Strings to various date formats.  These Strings
 * can be any well known format of date / time.
 */
public class CalendarFactory {

    private static boolean inDebug = false;
    private static ICalendarFactory instance;
    private static ZoneId zoneId = ZoneOffset.UTC;

    /**
     * Convert a ZonedDateTime to a Calendar instance while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(ZonedDateTime zonedDateTime,
                                      String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(asLong(modified)));
        return cal;
    }

    /**
     * Convert a String date time to a Calendar instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(String dateTime,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to a Calendar instance while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(LocalDateTime dateTime,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asCalendar(LocalTime time,
                                  String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a calendar"))
                .build();
    }

    /**
     * Convert a local date to a Calendar instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(LocalDate date,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a Calendar instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(long epochMillisecond,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to a Calendar instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(Calendar calendar,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a Calendar instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the calendar
     */
    static public Calendar asCalendar(Date date,
                                      String... adjustmentsArray) {
        return asCalendar(asZoned(date, adjustmentsArray));
    }


    /**
     * Convert a zoned date time to a LocalDateTime instance while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(ZonedDateTime zonedDateTime,
                                                String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return modified.toLocalDateTime();
    }

    /**
     * Convert a String date time to a LocalDateTime instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(String dateTime,
                                                String... adjustmentsArray) {
        return asLocalDateTime(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to a LocalDateTime instance while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(LocalDateTime dateTime,
                                                String... adjustmentsArray) {
        return asLocalDateTime(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date to a LocalDate instance while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDateTime asLocalDateTime(LocalDate dateTime,
                                            String... adjustmentsArray) {
        return asLocalDateTime(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asLocalDateTime(LocalTime time,
                                       String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a local date time"))
                .build();
    }

    /**
     * Convert epoch milliseconds to a LocalDateTime instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(long epochMillisecond,
                                                String... adjustmentsArray) {
        return asLocalDateTime(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to a LocalDateTime instance while optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(Calendar calendar,
                                                String... adjustmentsArray) {
        return asLocalDateTime(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a LocalDateTime instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the local date time
     */
    static public LocalDateTime asLocalDateTime(Date date,
                                                String... adjustmentsArray) {
        return asLocalDateTime(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a Zoned date time to a LocalDate while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(ZonedDateTime zonedDateTime,
                                        String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return modified.toLocalDate();
    }

    /**
     * Convert a String date time to a LocalDate instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(String dateTime,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to a LocalDate instance while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(LocalDateTime dateTime,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asLocalDate(LocalTime time,
                                   String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a local date"))
                .build();
    }

    /**
     * Convert a local date to a LocalDate instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(LocalDate date,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a LocalDate instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(long epochMillisecond,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to a LocalDate instance while optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(Calendar calendar,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a LocalDate instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the local date
     */
    static public LocalDate asLocalDate(Date date,
                                        String... adjustmentsArray) {
        return asLocalDate(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a Zoned date time to a LocalTime while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(ZonedDateTime zonedDateTime,
                                        String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return modified.toLocalTime();
    }

    /**
     * Convert a String date time to a LocalTime instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(String dateTime,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to a LocalTime instance while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(LocalDateTime dateTime,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * As local time local time.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(LocalTime time,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(time, adjustmentsArray));
    }

    /**
     * Convert a local date to a LocalTime instance while optionally
     * applying adjustments.  The time is low value.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(LocalDate date,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a LocalTime instance while optionally
     * applying adjustments.  The date portion will be removed.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(long epochMillisecond,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to a LocalTime instance while optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(Calendar calendar,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a LocalTime instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the local time
     */
    static public LocalTime asLocalTime(Date date,
                                        String... adjustmentsArray) {
        return asLocalTime(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a ZonedDateTime to a Date instance while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(ZonedDateTime zonedDateTime,
                              String... adjustmentsArray) {
        try {
            ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
            if (modified.toLocalDate() == null
                  || modified.toLocalDate() == LocalDate.MIN
                  || modified.toLocalDate() == LocalDate.MAX) {
                ZonedDateTime wDate = ZonedDateTime.of(LocalDate.MIN,
                                                       modified.toLocalTime(),
                                                       modified.getZone());
                return Date.from(wDate.toInstant());
            }
            return Date.from(modified.toInstant());
        } catch (Exception e) {
            return asDate("now", "=1day =1month =0year");
        }
    }

    /**
     * Convert a String date time to a Date instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(String dateTime,
                              String... adjustmentsArray) {
        return asDate(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to a Date instance while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(LocalDateTime dateTime,
                              String... adjustmentsArray) {
        return asDate(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asDate(LocalTime time,
                              String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a date"))
                .build();
    }

    /**
     * Convert a local date to a Date instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(LocalDate date,
                              String... adjustmentsArray) {
        return asDate(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a Date instance while optionally
     * applying adjustments.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(long epochMillisecond,
                              String... adjustmentsArray) {
        return asDate(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to a Date instance while optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(Calendar calendar,
                              String... adjustmentsArray) {
        return asDate(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a Date instance while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the date
     */
    static public Date asDate(Date date,
                              String... adjustmentsArray) {
        return asDate(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a ZonedDateTime to a epoch milliseconds while removing
     * the time part and optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(ZonedDateTime zonedDateTime,
                                  String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return asLong(ZonedDateTime.of(modified.toLocalDate(),
                                       LocalTime.MIN,
                                       modified.getZone()));
    }

    /**
     * Convert a String date time to an epoch milliseconds of only the date part
     * while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(String dateTime,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to an epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(LocalDateTime dateTime,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asDateLong(LocalTime time,
                                  String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a date long"))
                .build();
    }

    /**
     * Convert a local date to an epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(LocalDate date,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(long epochMillisecond,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a date to an epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(Calendar calendar,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to an epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asDateLong(Date date,
                                  String... adjustmentsArray) {
        return asDateLong(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a Zoned date time to a epoch milliseconds while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(ZonedDateTime zonedDateTime,
                              String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        long seconds = modified.toEpochSecond();
        return (seconds * 1000) + modified.getNano() / 1000000;
    }

    /**
     * Convert a String date time to an epoch milliseconds while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(String dateTime,
                              String... adjustmentsArray) {
        return asLong(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date time to an epoch milliseconds while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(LocalDateTime dateTime,
                              String... adjustmentsArray) {
        return asLong(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date to a epoch milliseconds while optionally
     * applying adjustments.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(LocalDate date,
                              String... adjustmentsArray) {
        return asLong(asZoned(date, adjustmentsArray));
    }

    /**
     * As long.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asLong(LocalTime time,
                              String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a long"))
                .build();
    }

    /**
     * Convert epoch milliseconds to an epoch milliseconds while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(long epochMillisecond,
                              String... adjustmentsArray) {
        return asLong(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a calendar to an epoch milliseconds while
     * removing the time part and optionally
     * applying adjustments.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(Calendar calendar,
                              String... adjustmentsArray) {
        return asLong(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to an epoch milliseconds while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the long
     */
    static public long asLong(Date date,
                              String... adjustmentsArray) {
        return asLong(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a calendar to a list of adjustments while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(Calendar calendar,
                                   String... adjustmentsArray) {
        return getInstance().asFormula(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date instance to a list of adjustments while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(Date date,
                                   String... adjustmentsArray) {
        return asFormula(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert a String date time to a list of adjustments while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(String dateTime,
                                   String... adjustmentsArray) {
        return asFormula(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date to a list of adjustments while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(LocalDate date,
                                   String... adjustmentsArray) {
        return asFormula(asZoned(date, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void asFormula(LocalTime time,
                                 String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to convert a time to a formula"))
                .build();
    }

    /**
     * Convert epoch milliseconds to a list of adjustments while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(long epochMillisecond,
                                   String... adjustmentsArray) {
        return asFormula(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a Zoned date time to a list of adjustments while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(ZonedDateTime zonedDateTime,
                                   String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return getInstance().asFormula(modified);
    }

    /**
     * Convert a local date time to a list of adjustments while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asFormula(LocalDateTime dateTime,
                                   String... adjustmentsArray) {
        return getInstance().asFormula(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a calendar to a JSON formatted String while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(Calendar calendar,
                                String... adjustmentsArray) {
        return asJSON(asZoned(calendar, adjustmentsArray));
    }

    /**
     * Convert a date to a JSON formatted String while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(Date date,
                                String... adjustmentsArray) {
        return asJSON(asZoned(date, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a JSON formatted String while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(long epochMillisecond,
                                String... adjustmentsArray) {
        return asJSON(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Convert a String date time to a JSON formatted date / time while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(String dateTime,
                                String... adjustmentsArray) {
        return asJSON(asZoned(dateTime, adjustmentsArray));
    }

    /**
     * Convert a local date to a JSON formatted String while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(LocalDate date,
                                String... adjustmentsArray) {
        ZonedDateTime modified = asZoned(date, adjustmentsArray);
        return DateTimeFormatter.ISO_LOCAL_DATE.format(modified.toLocalDate());
    }

    /**
     * As json string.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(LocalTime time,
                                String... adjustmentsArray) {
        ZonedDateTime modified = asZoned(time, adjustmentsArray);
        return DateTimeFormatter.ISO_LOCAL_TIME.format(modified.toLocalTime());
    }

    /**
     * Convert a Zoned date time to a JSON formatted String while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(ZonedDateTime zonedDateTime,
                                String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return DateTimeFormatter.ISO_ZONED_DATE_TIME.format(modified);
    }

    /**
     * Convert a local date time to a JSON formatted String while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param ldt              the ldt
     * @param adjustmentsArray the adjustments array
     * @return the string
     */
    static public String asJSON(LocalDateTime ldt,
                                String... adjustmentsArray) {
        ZonedDateTime modified = asZoned(ldt, adjustmentsArray);
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(modified);
    }

    /**
     * Convert a calendar to a ZonedDateTime instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param calendar         the calendar
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(Calendar calendar,
                                        String... adjustmentsArray) {
        ZonedDateTime original = calendar.toInstant().atZone(zoneId);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * Convert a date to a ZonedDateInstance instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(Date date,
                                        String... adjustmentsArray) {
        ZonedDateTime original = date.toInstant().atZone(zoneId);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * Convert a local date to a ZonedDateTime instance while optionally
     * applying adjustments.   The time part will be lower bounds.
     * The zone will be applied according to the setZone method.
     *
     * @param date             the date
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(LocalDate date,
                                        String... adjustmentsArray) {
        ZonedDateTime original = ZonedDateTime.of(date,
                                                  LocalTime.MIN,
                                                  zoneId);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * Convert a Zoned date time to a ZonedDateTime while optionally
     * applying adjustments.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(ZonedDateTime dateTime,
                                        String... adjustmentsArray) {
        return getInstance().modifyImpl(dateTime, adjustmentsArray);
    }

    /**
     * Convert a local date time to a ZonedDateTime instance while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param dateTime         the date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(LocalDateTime dateTime,
                                        String... adjustmentsArray) {
        ZonedDateTime original = ZonedDateTime.of(dateTime, zoneId);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * As zoned zoned date time.
     *
     * @param time             the time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(LocalTime time,
                                        String... adjustmentsArray) {
        ZonedDateTime original = ZonedDateTime.of(LocalDate.MIN,
                                                  time,
                                                  zoneId);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * Convert a String date time to a ZonedDateTime instance while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param datetime         the datetime
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(String datetime,
                                        String... adjustmentsArray) {
        try {
            return getInstance().modifyImpl(
              TemporalHelper.parseWithPredefinedParsers(datetime,
                                                        getInstance().getOverrideForSystemTime(),
                                                        zoneId),
              adjustmentsArray);
        } catch (Exception e) {
            throw CalendarFactoryException.builder().cause(e).build();
        }
    }

    /**
     * Convert epoch milliseconds to a ZonedDateTime instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime asZoned(long epochMillisecond,
                                        String... adjustmentsArray) {
        Instant instant = Instant.ofEpochSecond(0, epochMillisecond * 1000000);
        ZonedDateTime original = instant.atZone(ZoneOffset.UTC);
        return getInstance().modifyImpl(original, adjustmentsArray);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    static public ICalendarFactory getInstance() {
        if (instance == null) {
            instance = new CalendarFactoryImpl();
        }
        return instance;
    }

    /**
     * Is in debug boolean.
     *
     * @return the boolean
     */
    public static boolean isInDebug() {
        return inDebug;
    }

    /**
     * Convert a calendar to a ZonedDateTime instance while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param startingDateTime the starting date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(Calendar startingDateTime,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(startingDateTime, adjustmentsArray));
    }

    /**
     * Convert a date to a ZonedDateTime instance while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param startingDate     the starting date
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(Date startingDate,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(startingDate, adjustmentsArray));
    }

    /**
     * Convert a Zoned date time to a ZonedDateTime  while
     * removing the time part and optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(ZonedDateTime zonedDateTime,
                                       String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        return getInstance().noTimeImpl(modified);
    }

    /**
     * Convert a local date time to a ZonedDateTime instance while
     * removing the time part and optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param startingDateTime the starting date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(LocalDateTime startingDateTime,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(startingDateTime, adjustmentsArray));
    }

    /**
     * Convert a local date to a ZonedDateTime instance while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param startingDate     the starting date
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(LocalDate startingDate,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(startingDate, adjustmentsArray));
    }

    /**
     * This method provides nothing since the concept is nonsense.  But
     * it is here to be a be a reminder to not try to do this.
     *
     * @param startingTime     the starting time
     * @param adjustmentsArray the adjustments array
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unused")
    static public void noTime(LocalTime startingTime,
                              String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to remove time from a time"))
                .build();
    }

    /**
     * Convert a String date time to a ZonedDateTime instance while
     * removing the time portion and optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param startingDateTime the starting date time
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(String startingDateTime,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(startingDateTime, adjustmentsArray));
    }

    /**
     * Convert epoch milliseconds to a ZonedDateTime instance while
     * removing the time part and optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param epochMillisecond the epoch millisecond
     * @param adjustmentsArray the adjustments array
     * @return the zoned date time
     */
    static public ZonedDateTime noTime(long epochMillisecond,
                                       String... adjustmentsArray) {
        return getInstance().noTimeImpl(asZoned(epochMillisecond, adjustmentsArray));
    }

    /**
     * Reset calendar factory.
     *
     * @param newFactory the new factory
     * @return the calendar factory
     */
    static public ICalendarFactory reset(ICalendarFactory newFactory) {
        ICalendarFactory oldFactory = instance;
        instance = newFactory;
        return oldFactory;
    }

    /**
     * Sets the business date to a ZonedDateTime while optionally
     * applying adjustments.
     *
     * @param zonedDateTime    the zoned date time
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(ZonedDateTime zonedDateTime,
                                       String... adjustmentsArray) {
        ZonedDateTime modified = getInstance().modifyImpl(zonedDateTime, adjustmentsArray);
        getInstance().setOverrideForSystemTime(modified);
    }

    /**
     * Sets the business date to a LocalDateTime instance while optionally
     * applying adjustments.
     *  The zone will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(LocalDateTime businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Set the business date to a LocalDate while optionally
     * applying adjustments.  The time will be lower bounds.
     * The zone will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(LocalDate businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Sets business date.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    @SuppressWarnings("unused")
    static public void setBusinessDate(LocalTime businessDate,
                                       String... adjustmentsArray) {
        throw CalendarFactoryException.builder()
                .cause(new Exception("invalid to set a business date to a time"))
                .build();
    }

    /**
     * Sets the business date to a Date while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(Date businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Set the business date from a Calendar instance while optionally
     * applying adjustments.
     * The zone will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(Calendar businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Set the business date from an epoch milliseconds while optionally
     * applying adjustments.  The zone
     * will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(long businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Set the business date from a String date / time while optionally
     * applying adjustments.  The zone, if not part of the date string,
     * will be applied according to the setZone method.
     *
     * @param businessDate     the business date
     * @param adjustmentsArray the adjustments array
     */
    static public void setBusinessDate(String businessDate,
                                       String... adjustmentsArray) {
        getInstance().setOverrideForSystemTime(asZoned(businessDate, adjustmentsArray));
    }

    /**
     * Sets in debug.
     *
     * @param inDebug_parm the in debug parm
     */
    static public void setInDebug(boolean inDebug_parm) {
        inDebug = inDebug_parm;
    }

    /**
     * Sets zone id.  The default is UTC.
     *
     * @param newZoneId the new zone id
     * @return the zone id
     */
    static public ZoneId setZoneId(ZoneId newZoneId) {
        ZoneId previousZoneId = zoneId;
        zoneId = newZoneId;
        return previousZoneId;
    }
}
