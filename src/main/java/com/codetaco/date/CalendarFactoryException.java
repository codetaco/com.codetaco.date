package com.codetaco.date;

/**
 * This is the only exception type that is thrown from the CalendarFactory.  It
 * is a runtime exception so that it does not need to be caught unless you want
 * to.
 */
public class CalendarFactoryException extends RuntimeException {

    /**
     * This class is the builder class for all CalendarFactoryException
     * instances.
     */
    public static class Builder {

        private Throwable cause;


        /**
         * Store the cause exception that this instance is wrapping.
         *
         * @param cause the cause
         * @return the builder
         */
        public Builder cause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        /**
         * Build calendar factory exception.
         *
         * @return the calendar factory exception
         */
        public CalendarFactoryException build() {
            if (cause instanceof CalendarFactoryException) {
                return (CalendarFactoryException) cause;
            }
            return new CalendarFactoryException(cause);
        }
    }

    /**
     * Return a builder to make an instance of the {@link CalendarFactoryException}.
     *
     * @return the builder
     */
    static public Builder builder() {
        return new Builder();
    }

    private CalendarFactoryException() {
    }

    private CalendarFactoryException(String message) {
        super(message);
    }

    private CalendarFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

    private CalendarFactoryException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return getCause().getMessage();
    }

    private CalendarFactoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
