.. _api-asZoned:

asZoned()
=========

This method is used to apply adjustments to a starting date.
The result will be a ZonedDateTime instance.


.. code:: java

    static public ZonedDateTime asZoned(Calendar startingDate,
                                        String... adjustmentsArray)

:startingDate: The starting date to which modifications will be processed.
    Shown above is a Calendar type for startingDate.
    However it can also be any one of many others too.

    .. hlist::
        :columns: 3

        * Date
        * Calendar
        * LocalDate
        * LocalTime
        * LocalDateTime
        * ZonedDateTime
        * String
        * Long

:adjustmentsArray: The :ref:`adjustmentsArray<api-modify-adjustments>` is an array of type String
    that specifies adjustments to be made to the startingDate.
    One or more modifications can be applied and will be applied in the order that they are specified.

    Briefly, adjustments are <direction><quantity><unit>.

    Direction
        +, -, =, >, <, >=, <=

    Quantity
        a position integer, B, E

    Unit
        year, month, day, dow, hour, minute, second, millisecond, nanosecond, time

