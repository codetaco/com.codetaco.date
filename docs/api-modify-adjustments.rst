.. _api-modify-adjustments:

Adjustments
===========

Adjustments allow you to modify the starting date as it is transformed into the modified returned date.
Each adjustment must be specified with this pattern::

    <direction><quantity><unit>

The adjustment parameter is an array of Strings.
Each string is an instance of the above pattern.  Each element
in the array will be applied to the date in the same sequence that it appears in the array.

.. warning::

    There may be no spaces within a single pattern.

.. tip::

    **There is flexibility in making adjustments.**
    All of the elements of the array may be specified in a
    single string separated by space(s).

Direction
---------
The direction pertains to the action of the command.  The other two parts of
a pattern (quantity and unit) are modifiers on the direction.
The direction indicates whether your intent is to increase the date, decrease the date,
or set the date to something specific.

Increase the date / time
++++++++++++++++++++++++

:\+: The direction sign (+) will modify the date / time into the future
    according to the remainder of the pattern.

    ex: :code:`+1day +10years +14minutes`

Decrease the date / time
++++++++++++++++++++++++

:\-: The direction sign (-) will modify the date / time into the past
    according to the remainder of the pattern.

    ex: :code:`-3days -1month -150seconds`

Set the date / time
+++++++++++++++++++

:\=: The direction sign (=) will modify the date / time to be a specific value
    according to the remainder of the pattern.

    ex: :code:`=0hours =0minutes =0seconds =0nanoseconds`

Sliding Directions
++++++++++++++++++
There are also a few advanced directions that do about the same thing as the normal (`+-=`) directions
except they more-or-less roll, or slide, into the modification.
The effect is that lower units will be reset to an initial value.
As an example :code:`>3h` will move the date forward to 3AM and then set the hour, minute, second, and nanoseconds
to 0.

Sliding a date forward or backward means to

1. start at the starting date
2. set the lower units to appropriate low values
3. increase / decrease the requested unit until the adjustment has been reached.

If the current time is 5AM and the pattern
indicated :code:`>3h` then the day will also be increased as the date slides forward. 5AM (the current
time) is already greater than the specified 3AM.  So the time will slide from 6AM - 7AM ... 3AM.

:\>: Slide forward to the date time specified in the remainder of the pattern.  All lower units will be set to an
    appropriate low value.

:\<: Slide backwards to the date time specified in the remainder of the pattern.  All lower units will be set to an
    appropriate high value.  Ex: :code:`<3h` and the current time is 5AM.  The result of the slide
    is that the hour will become 3AM.
    As the slide proceeds backwards, the first time that 3AM is reached is at 03:59:59.999.

:>=: This is very similar to :code:`>` except that the current value for the same unit will be considered.
    Sliding from 3AM with the pattern of :code:`>=3H` will return 03:00:00.000 of the same day.

:<=: This is very similar to :code:`<` except that the current value for the same unit will be considered.
    Sliding from 3AM with the pattern of :code:`<=3H` will return 03:00:00.000 of the same day.

.. warning::

    Sliding adjustments can go backwards!
    The effect of setting the lower units to 0 before applying the sliding adjustment is that even though
    the request was to slide forward the time may move to the beginning of the unit.
    03:45 becomes 3:00 when applying :code:`>=3H`.

Quantity
--------

Numeric Quantity
++++++++++++++++
The quantity is usually a positive integer or zero.  In the pattern it is the amount of the unit that is being adjusted.
In `=3min` the 3 is the quantity of minutes.  This pattern would set the minute part of the time to be 03.

Quantity as Beginning or Ending
+++++++++++++++++++++++++++++++
Rather than using an integer as a quantity there are two special characters that can be used instead to handle
unique situations.

:\B: Set the specified unit to the beginning (lowest possible) value.  This is not meaningful (or allowed)
    on the (`+-`) directions.
    To set the day to the beginning of the current month a pattern of :code:`=Bmonth` would do it.
    In this case it would also set the lower units in the time to low-values.

:\E: Set the specified unit to the ending (highest possible) value.  This is not meaningful (or allowed)
    on the (`+-`) directions.
    To set the day to the end of the current month a pattern of :code:`=Emonth` would do it.
    In this case it would also set the lower units in the time to high-values.

    .. tip::

        **Setting the date/time to be the beginning of the last day of the month.**
        First set the date to the end of the month and then set it to the beginning of the day.
        This takes two patterns: :code:`=eMonth =bDay`.  The order of these patterns is important.
        If they were reversed the effect on the result would be different.

Unit
----
The unit is the last part of each pattern.  In general the unit may be specified as the entire word or any
beginning portion of the word that makes it unique.

.. warning::

    **month, minute, and millisecond conflict** if only one character would be used to specify them as
    a unit. In this specific case, "m" will mean "month".  See each unit description for these types
    of exceptions.

.. tip::

    Case is not significant.  Year is the same as YeAr.

:year: This refers to the century and year of the date.
    :code:`=1960year` sets the century and year of the date to be 1960.

    Abbreviations: any word starting with "y".

:month: This refers to the month of the date.
    :code:`=8Month` sets the month to August.

    Abbreviations: any word starting with "m",
    except "ms" and "mi".  Those are used for milliseconds and minutes.

:day: This refers to the day of the month of the date.
    :code:`=15day` sets the day part of the date to 15.

    Abbreviations: any word starting with "d" except for "dayofweek" and "dow".

:day of week: This refers to the day of the week as in Monday, Tuesday ...
    :code:`=2dow` sets the day of the month to match Tuesday of the current week.

    .. tip::

        Monday is the 1st day of the week, Sunday is the last (7).

    Abbreviations: dayOfWeek, dow.

:hour: This refers to the hour of the day where 0 is midnight and 23 is 11PM.
    :code:`=14hour` sets the hour of the day to 2PM.

    Abbreviations: any word starting with "h".

:minute: This refers to the minute of the hour where 0 is the first minute of an hour and 59 is the last.
    :code:`=30mi` sets the minute of the current hour to be :30.

    Abbreviations: any word starting with "mi".

:second: This refers to the second of the minute where 0 is the first second of an hour and 59 is the last.
    :code:`=10second` sets the second of the current minute to :10.

    Abbreviations: any word starting with "s".

:millisecond: This refers to the millisecond of the second where 0 is the first millisecond of a second and 999 is the last.
    :code:`=75ms` sets the millisecond of the current second to be .075.

    Abbreviations: any word starting with "l", also "ms".

:nanosecond: This refers to the nanosecond of the second where 0 is the first nanosecond of a second and 999999999 is the last.
    :code:`=12345n` sets the nanosecond of the current second to be .000012345.

    Abbreviations: any word starting with "n".

:time: This is a special case unit that only allows 2 uses: `=BTime` and `=ETime`
    giving 0:0:0.0 and 23:59:59.999 respectively.

    Abbreviations: any word starting with "t".



