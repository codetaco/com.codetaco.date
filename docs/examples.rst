.. _examples:

Examples
========
These examples are shown as snippets of jUnit test cases.
They can be run from the test class :ref:`com.codetaco.date.ExamplesOnReadTheDocs<https://bitbucket.org/codetaco/com.codetaco.date/src/master/src/test/java/com/codetaco/date/ExamplesOnReadTheDocs.java>`
In most of the examples we are causing the system date / time to be a specific UTC time so that
it is predicatable what the result will be for ``now``.

Date and time for right now
---------------------------

.. code:: java

        CalendarFactory.setBusinessDate(CalendarFactory.convert("1960-04-09T12:15:47.321Z"));
        ZonedDateTime result = CalendarFactory.now();
        Assert.assertEquals("1960-04-09T12:15:47.321Z", CalendarFactory.asJSON(result));

Now rounded to the start of the hour
------------------------------------

.. code:: java

        CalendarFactory.setBusinessDate(CalendarFactory.convert("1960-04-09T12:15:47.321Z"));
        ZonedDateTime result = CalendarFactory.now("=bhour");
        Assert.assertEquals("1960-04-09T12:00:00.000Z", CalendarFactory.asJSON(result));


What time is it in Chicago
--------------------------

.. code:: java

        CalendarFactory.setBusinessDate(CalendarFactory.convert("1960-04-09T12:15:47.321-00:00"));
        ZonedDateTime result = CalendarFactory.now()
        .withZoneSameInstant(ZoneId.of("America/Chicago"));
        Assert.assertEquals("1960-04-09T07:15:47.321-06:00", CalendarFactory.asJSON(result));

What local time is it when I know some other zone's time
--------------------------------------------------------
The time in Hyderabad, Telangana, India (GMT+5:30) is known.  What is my local time.

.. code:: java

        CalendarFactory.setBusinessDate(CalendarFactory.convert("2018-10-20T10:00+05:30"));
        LocalTime result = CalendarFactory.asLocalTime(
          CalendarFactory.now().withZoneSameInstant(ZoneId.of("America/Chicago")));
        Assert.assertEquals("23:30:00.000", CalendarFactory.asJSON(result));
