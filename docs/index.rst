Date by CodeTaco
================

.. image:: /images/codetaco.png
    :width: 120px
    :alt: CodeTaco logo
    :align: right

.. image:: /images/date.jpg
    :width: 120px
    :alt: Date logo
    :align: left

"Date" is a library to deal with Java date and time objects.
It is not a new type of date or time, instead it provide a focused interface to control
the aspects of Java's standard date and time objects that are necessary in modern applications.

Whatever the Java API is, "Date" provides a constant API for dealing with it.

This library allows an easy migration path from Calendar to ZonedDateTime.
It also provides a simple language for applying modifications to a date/time to come up with a
new date/time as a result. Like changing a date to be the end of the same month.

Things to do
------------

`Read the Javadocs <http://www.javadoc.io/doc/com.codetaco/date>`_

`Get the code to include this in your project's dependencies <https://search.maven.org/search?q=g:com.codetaco%20AND%20a:date&core=gav>`_


:ref:`Keyword Index <genindex>`, :ref:`Search Page <search>`

.. toctree::
    :maxdepth: 3
    :caption: Contents:

    api.rst
    api-asCalendar.rst
    api-asDateLong.rst
    api-asFormula.rst
    api-asJSON.rst
    api-asLong.rst
    api-asZoned.rst
    api-noTime.rst
    api-setBusinessDate.rst
    api-modify-adjustments.rst
    examples.rst
