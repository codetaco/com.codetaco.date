API for the Date Library
========================

There are only a few basic API entry points into the Date library.
They all exist as static methods on the ``CalendarFactory`` class.

The first parameter to all of the API entrypoints is a date.  It can
be in many formats.  The String format is the most flexible.

.. tip::

    Two special date Strings are "now" and "today".  The later has no time portion.

Summary
-------

.. tip::

    The only difference between most of the API methods are the overloaded parameters that they accept.

:ref:`The as calendar methods<api-asCalendar>`

.. code:: Java

    static public Calendar asCalendar(Calendar calendar, String... adjustmentsArray);
    static public Calendar asCalendar(Date date, String... adjustmentsArray);
    static public Calendar asCalendar(String dateTime, String... adjustmentsArray);
    static public Calendar asCalendar(LocalDate date, String... adjustmentsArray);
    static public Calendar asCalendar(LocalTime time, String... adjustmentsArray);
    static public Calendar asCalendar(LocalDateTime calendar, String... adjustmentsArray);
    static public Calendar asCalendar(ZonedDateTime calendar, String... adjustmentsArray);
    static public Calendar asCalendar(long epochMillisecond, String... adjustmentsArray);

:ref:`The as date methods<api-asDate>`

.. code:: Java

    static public Date asDate(Calendar calendar, String... adjustmentsArray);
    static public Date asDate(Date date, String... adjustmentsArray);
    static public Date asDate(String dateTime, String... adjustmentsArray);
    static public Date asDate(LocalDate date, String... adjustmentsArray);
    static public Date asDate(LocalTime time, String... adjustmentsArray);
    static public Date asDate(LocalDateTime calendar, String... adjustmentsArray);
    static public Date asDate(ZonedDateTime calendar, String... adjustmentsArray);
    static public Date asDate(long epochMillisecond, String... adjustmentsArray);

:ref:`The as date long methods<api-asDateLong>`

.. code:: Java

    static public long asDateLong(Calendar calendar, String... adjustmentsArray);
    static public long asDateLong(Date date, String... adjustmentsArray);
    static public long asDateLong(String dateTime, String... adjustmentsArray);
    static public long asDateLong(LocalDate date, String... adjustmentsArray);
    static public long asDateLong(LocalTime time, String... adjustmentsArray);
    static public long asDateLong(LocalDateTime calendar, String... adjustmentsArray);
    static public long asDateLong(ZonedDateTime calendar, String... adjustmentsArray);
    static public long asDateLong(long epochMillisecond, String... adjustmentsArray);

:ref:`The as long methods<api-asLong>`

.. code:: Java

    static public long asLong(Calendar calendar, String... adjustmentsArray);
    static public long asLong(Date date, String... adjustmentsArray);
    static public long asLong(String dateTime, String... adjustmentsArray);
    static public long asLong(LocalDate date, String... adjustmentsArray);
    static public long asLong(LocalTime time, String... adjustmentsArray);
    static public long asLong(LocalDateTime calendar, String... adjustmentsArray);
    static public long asLong(ZonedDateTime calendar, String... adjustmentsArray);
    static public long asLong(long epochMillisecond, String... adjustmentsArray);

:ref:`The as formula methods<api-asFormula>`

.. code:: Java

    static public String asFormula(Calendar calendar, String... adjustmentsArray);
    static public String asFormula(Date date, String... adjustmentsArray);
    static public String asFormula(String dateTime, String... adjustmentsArray);
    static public String asFormula(LocalDate date, String... adjustmentsArray);
    static public String asFormula(LocalTime time, String... adjustmentsArray);
    static public String asFormula(LocalDateTime calendar, String... adjustmentsArray);
    static public String asFormula(ZonedDateTime calendar, String... adjustmentsArray);
    static public String asFormula(long epochMillisecond, String... adjustmentsArray);

:ref:`The as JSON methods<api-asJSON>`

.. code:: Java

    static public String asJSON(Calendar calendar, String... adjustmentsArray);
    static public String asJSON(Date date, String... adjustmentsArray);
    static public String asJSON(String dateTime, String... adjustmentsArray);
    static public String asJSON(LocalDate date, String... adjustmentsArray);
    static public String asJSON(LocalTime time, String... adjustmentsArray);
    static public String asJSON(LocalDateTime ldt, String... adjustmentsArray);
    static public String asJSON(ZonedDateTime ldt, String... adjustmentsArray);
    static public String asJSON(long epochMillisecond, String... adjustmentsArray);

:ref:`The as zoned date time methods<api-asZoned>`

.. code:: Java

    static public ZonedDateTime asZoned(Calendar calendar, String... adjustmentsArray);
    static public ZonedDateTime asZoned(Date date, String... adjustmentsArray);
    static public ZonedDateTime asZoned(LocalDate date, String... adjustmentsArray);
    static public ZonedDateTime asZoned(LocalTime time, String... adjustmentsArray);
    static public ZonedDateTime asZoned(LocalDateTime dateTime, String... adjustmentsArray);
    static public ZonedDateTime asZoned(ZonedDateTime dateTime, String... adjustmentsArray);
    static public ZonedDateTime asZoned(String datetime, String... adjustmentsArray);
    static public ZonedDateTime asZoned(long epochMillisecond, String... adjustmentsArray);

:ref:`The no-time methods<api-noTime>`

.. code:: Java

    static public ZonedDateTime noTime(Calendar startingDateTime, String... adjustmentsArray);
    static public ZonedDateTime noTime(Date startingDate, String... adjustmentsArray);
    static public ZonedDateTime noTime(LocalDate startingDate, String... adjustmentsArray);
    static public ZonedDateTime noTime(LocalTime startingTime, String... adjustmentsArray);
    static public ZonedDateTime noTime(LocalDateTime startingDateTime, String... adjustmentsArray);
    static public ZonedDateTime noTime(ZonedDateTime startingDateTime, String... adjustmentsArray);
    static public ZonedDateTime noTime(String startingDateTime, String... adjustmentsArray);
    static public ZonedDateTime noTime(long epochMillisecond, String... adjustmentsArray);

:ref:`The business date methods<api-setBusinessDate>`

.. code:: Java

    static public ZonedDateTime setBusinessDate(Calendar date, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(Date date, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(LocalDate date, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(LocalTime time, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(LocalDateTime dateTime, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(ZonedDateTime dateTime, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(String datetime, String... adjustmentsArray);
    static public ZonedDateTime setBusinessDate(long epochMillisecond, String... adjustmentsArray);

